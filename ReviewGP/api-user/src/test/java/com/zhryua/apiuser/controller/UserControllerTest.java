package com.zhryua.apiuser.controller;

import com.zhryua.allcommon.constant.CommonStatusEnum;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserControllerTest {

    @Autowired
    private UserController userController;

    @Test
    public void loginUserTest() {

    }

    @Test
    public void registerUser() {
        InsertUserRequest request = new InsertUserRequest();
        request.setPhone("13499872235")
                .setPassword("123098");
        ResponseResult result = userController.registerUser(request);
        if(result.getCode() == CommonStatusEnum.SUCCESS.getCode()) {
            System.out.println("成功");
        } else {
            System.out.println("失败");
        }
    }

}
