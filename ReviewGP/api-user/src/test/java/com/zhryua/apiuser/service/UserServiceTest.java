package com.zhryua.apiuser.service;

import com.zhryua.allcommon.constant.CommonStatusEnum;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import com.zhryua.allcommon.pojo.database.User;
import com.zhryua.allcommon.util.IdWorker;
import com.zhryua.allcommon.util.ToJson;
import com.zhryua.apiuser.newservice.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class UserServiceTest {

    private static IdWorker idWorker = new IdWorker();

    // 测试用user实例
    private static User user = new User();

    static {
        user.setBizId("biz-123456")
                .setName("zhryua")
                .setPhone("12345678901")
                .setPassword("123456")
                .setEmail("123@qq.com")
                .setQq("345")
                .setWechat("678")
                .setDepartment(12)
                .setClassNumber(1801)
                .setIdentity("826278439827176128782")
                .setIs_authentication(0)
                .setCtm(new Date())
                .setUtm(new Date())
                .setStatus(0)
                .setCardId("C716516176")
                .setAuditorId("12")
                .setSubApp(1)
                .setDormitoryId("232")
                .setBed((short)4)
                .setUserId("U71778389821")
                .setImage("jhuibfcvewiuesbfviubweiugfbvi.jpg");
    }

    @Autowired
    private UserServiceImpl userService;

    @Test
    public void login_test_1(){
    }

    @Test
    public void register_test_1() {
        InsertUserRequest request = new InsertUserRequest();
        BeanUtils.copyProperties(user, request);
        request.setBizId(idWorker.nextId()+"");
        String s = ToJson.toJson(request);
        System.out.println(s);

        ResponseResult result = userService.register(request);

        if(result.getCode() == CommonStatusEnum.SUCCESS.getCode()) {
            System.out.println("OK");
        }

    }


    @Test
    public void getCode_test_1() {

        String code = null;

        code = userService.getCodeByEmail("12331@qq.com");

        System.out.println(code);
    }
}
