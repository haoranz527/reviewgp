package com.zhryua.apiuser.service;

import com.zhryua.allcommon.dto.ResponseResult;

public interface IAuthService {

    public ResponseResult auth(String phone, String code);

}
