package com.zhryua.apiuser.service;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.apiuser.request.RegisterUserRequest;

public interface IRegisterUserService {

    public ResponseResult registerUser(RegisterUserRequest request);

}
