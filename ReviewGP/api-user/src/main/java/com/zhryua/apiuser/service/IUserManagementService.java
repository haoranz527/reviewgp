package com.zhryua.apiuser.service;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import org.springframework.stereotype.Service;

@Service
public interface IUserManagementService {

    /**
     * 注册用户
     * @param request
     * @return
     */
    public ResponseResult registerUser(InsertUserRequest request);

    /**
     * 逻辑删除用户，不是物理删除！！！
     * @return
     */
    public ResponseResult deleteUser();

}
