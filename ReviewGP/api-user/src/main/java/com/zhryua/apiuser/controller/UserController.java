package com.zhryua.apiuser.controller;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.UpdateUserRequest;
import com.zhryua.apiuser.newservice.impl.UserServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/pub/v1/api-user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

//    @Autowired
//    private IServiceVerificationCode codeService;

    @PostMapping("/login")
    public ResponseResult loginUser(@RequestParam String phone, @RequestParam String pwd) {
        return userService.login(phone, pwd);
    }
    @PostMapping("/logout")
    public ResponseResult logoutUser(@RequestParam String token) {
        return userService.logout(token);
    }

    @PostMapping("/modify")
    public ResponseResult modifyUser(@RequestBody UpdateUserRequest request) {
        return userService.modify(request);
    }

    @PostMapping("/register")
    public ResponseResult registerUser(@RequestBody InsertUserRequest request) {
        return userService.register(request);
    }

    /**
     * 获取验证码
     * @param path 获取的方式
     * @param type 1-邮件，2-手机
     */
    @GetMapping("/{type}/getCode")
    public String getCode(@RequestParam(value = "path") String path, @PathVariable("type") String type) {
        if(StringUtils.equals(type, "1")) {
            return userService.getCodeByEmail(path);
        } else {
            return userService.getCodeByPhone(path);
        }
    }

    @GetMapping("/{path}/verCode")
    public boolean verCode(@PathVariable(value = "path") String path, @RequestParam(value = "code") String code) {
        return userService.verCode(path, code);
    }
}
