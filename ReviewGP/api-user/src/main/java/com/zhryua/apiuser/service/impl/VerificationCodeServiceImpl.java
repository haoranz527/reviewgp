package com.zhryua.apiuser.service.impl;

import com.zhryua.allcommon.constant.CommonStatusEnum;
import com.zhryua.allcommon.constant.IdentityConstant;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceverificationcode.response.VerifyCodeResponse;
import com.zhryua.apiuser.service.IServiceSmsRestTemplateService;
import com.zhryua.apiuser.service.IServiceVerificationCodeRestTemplateService;
import com.zhryua.apiuser.service.IVerificationCodeService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VerificationCodeServiceImpl implements IVerificationCodeService {

    @Autowired
    private IServiceVerificationCodeRestTemplateService serviceVerificationCodeRestTemplateService;

    @Autowired
    private IServiceSmsRestTemplateService serviceSmsRestTemplateService;

    @Override
    public ResponseResult send(String phone) {

        ResponseResult responseResult = serviceVerificationCodeRestTemplateService.generatorCode(IdentityConstant.STUDENT, phone);

        VerifyCodeResponse verifyCodeResponse = null;
        // 成功获得验证码时，获取验证码并生成response
        if(responseResult.getCode() == CommonStatusEnum.SUCCESS.getCode()) {
            JSONObject data = JSONObject.fromObject(responseResult.getData().toString());
            verifyCodeResponse = (VerifyCodeResponse) JSONObject.toBean(data, VerifyCodeResponse.class);
        } else {
            return ResponseResult.fail("获取验证码错误");
        }

        String code = verifyCodeResponse.getCode();

        // 发送短信认证信息
        ResponseResult result = serviceSmsRestTemplateService.sendSms(phone, code);

        // 错误
        if(result.getCode() == CommonStatusEnum.FAIL.getCode()) {
            return ResponseResult.fail("发生错误");
        }

        return ResponseResult.success("成功");
    }

    @Override
    public ResponseResult verfty(String phone, String code) {
        return serviceVerificationCodeRestTemplateService.verifyCode(IdentityConstant.STUDENT, phone, code);
    }
}
