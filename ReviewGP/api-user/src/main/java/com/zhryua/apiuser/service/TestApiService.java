package com.zhryua.apiuser.service;

import com.zhryua.allcommon.api.ITestApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "service-user")
public interface TestApiService extends ITestApi {

}
