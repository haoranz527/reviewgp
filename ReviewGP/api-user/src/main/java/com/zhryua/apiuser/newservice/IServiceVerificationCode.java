package com.zhryua.apiuser.newservice;

import com.zhryua.allcommon.api.serviceverificationcode.IVerificationCodeService;
import com.zhryua.allcommon.fallbackFactory.serviceverificationcode.IServiceVerificationCodeFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

@FeignClient(name = "service-verification-code", fallbackFactory = IServiceVerificationCodeFactory.class)
@Service
public interface IServiceVerificationCode extends IVerificationCodeService {

}
