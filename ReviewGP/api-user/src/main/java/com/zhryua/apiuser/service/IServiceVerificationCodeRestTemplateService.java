package com.zhryua.apiuser.service;

import com.zhryua.allcommon.api.serviceverificationcode.IVerificationCodeService;
import org.springframework.cloud.openfeign.FeignClient;

//@FeignClient(name = "service-verification-code")
public interface IServiceVerificationCodeRestTemplateService extends IVerificationCodeService {

}
