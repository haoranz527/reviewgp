package com.zhryua.apiuser.newservice;

import com.zhryua.allcommon.api.servicecard.ICardService;
import com.zhryua.allcommon.fallbackFactory.servicecard.IServiceCardFallBackFactory;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "service-card", fallbackFactory = IServiceCardFallBackFactory.class)
public interface IServiceCard extends ICardService {
}
