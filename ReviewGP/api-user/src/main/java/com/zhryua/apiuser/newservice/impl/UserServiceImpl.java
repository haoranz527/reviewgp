package com.zhryua.apiuser.newservice.impl;

import com.alibaba.fastjson.JSON;
import com.zhryua.allcommon.constant.CommonStatusEnum;
import com.zhryua.allcommon.constant.TimeConstant;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicecard.request.InsertCardRequest;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.UpdateUserRequest;
import com.zhryua.allcommon.pojo.database.User;
import com.zhryua.apiuser.newservice.IServiceCard;
import com.zhryua.apiuser.newservice.IServiceUser;
import com.zhryua.apiuser.newservice.IServiceVerificationCode;
import com.zhryua.apiuser.newservice.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
@SuppressWarnings("all")
public class UserServiceImpl implements IUserService {
    @Autowired
    private IServiceUser serviceUser;

    @Autowired
    private IServiceCard serviceCard;

    @Autowired
    private IServiceVerificationCode codeService;

    @Override
    public ResponseResult login(String phone, String pwd) {
        log.info("进入login，{} - {}", phone, pwd);
        try{
            // 默认保持登录30天
            ResponseResult result = serviceUser.login(phone, pwd, TimeConstant.ONE_MONTH);

            String token = result.getData().toString();

            if(StringUtils.isBlank(token)) {
                log.info("token is null");
                return ResponseResult.fail("内部错误");
            }
            log.info("成功");
            return ResponseResult.success(token);

        } catch (Exception e) {
            log.info("捕捉到错误");
            return ResponseResult.fail("内部错误" + e);
        }

    }

    @Override
    public ResponseResult logout(String token) {
        log.info("进入logout, {}", token);
        try{
            ResponseResult result = serviceUser.logOut(token);

            if(result.getCode() == CommonStatusEnum.FAIL.getCode()) {
                log.error("发生错误");
                return ResponseResult.fail("内部错误");
            }

            log.info("成功");
            return ResponseResult.success("成功");

        } catch (Exception e) {
            log.error("内部错误, {}", e);
            return ResponseResult.fail(e);
        }
    }

    @Override
    public ResponseResult modify(UpdateUserRequest request) {
        log.info("进入modify，{}", request);
        try{
            ResponseResult result = serviceUser.updateUser(request);

            // 失败
            if(Objects.isNull(result)
                    || result.getCode() != CommonStatusEnum.SUCCESS.getCode()) {
                log.error("内部错误，{}");
            }

            return ResponseResult.success("成功");

        } catch (Exception e) {
            log.error("内部错误,{}", e);
            return ResponseResult.fail(e);
        }
    }

    @Override
    public ResponseResult register(InsertUserRequest request) {
        log.info("进入modify，{}", request);
        try{
            ResponseResult result = serviceUser.insertUser(request);

            // 失败
            if(Objects.isNull(result)
                    || result.getCode() != CommonStatusEnum.SUCCESS.getCode()) {
                log.error("内部错误，{}");
            }

//            User user = new User();
//            BeanUtils.copyProperties(result.getData(), user, User.class);
//            User user = new User();

            //=================================

            String s = JSON.toJSONString(result.getData());
            User user = JSON.parseObject(s, User.class);

            //=================================



            String userId = user.getUserId();
            String cardId = user.getCardId();

            InsertCardRequest cardRequest = new InsertCardRequest();
            cardRequest.setUserId(userId)
                    .setBizId(cardId)
                    .setLimit(9999999L)
                    .setBalance(0L);
             result = serviceCard.insertCard(cardRequest);
             if(result.getCode() != CommonStatusEnum.SUCCESS.getCode()) {
                 return ResponseResult.fail("失败");
             }


            return ResponseResult.success("成功");

        } catch (Exception e) {
            log.error("内部错误,{}", e);
            return ResponseResult.fail(e);
        }
    }

    public String getCodeByEmail(String path) {
        return codeService.getCodeByEmail(path);
    }

    public String getCodeByPhone(String phone) {
        return codeService.getCodeByPhone(phone);
    }

    public boolean verCode(String path, String code) {
        return codeService.verCode(path, code);
    }
}
