package com.zhryua.apiuser.service;

import com.zhryua.allcommon.api.serviceuser.IUserService;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient(name = "service-user")
public interface IServiceUserRestTemplate extends IUserService {


}
