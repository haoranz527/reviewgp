package com.zhryua.apiuser.service;

import com.zhryua.allcommon.dto.ResponseResult;

public interface IVerificationCodeService {
    public ResponseResult send(String phone);

    public ResponseResult verfty(String phone, String code);
}
