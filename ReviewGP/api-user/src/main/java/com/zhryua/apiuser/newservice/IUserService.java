package com.zhryua.apiuser.newservice;


import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.UpdateUserRequest;

/**
 * 用户端可以完成的动作：
 * 1、登录
 * 2、注销
 * 3、修改信息
 * 4、注册
 */

public interface IUserService {

    /**
     * 登录，此处仅实现手机号+密码登录
     * @return
     */
    public ResponseResult login(String phone, String pwd);

    public ResponseResult logout(String token);

    public ResponseResult modify(UpdateUserRequest request);

    public ResponseResult register(InsertUserRequest request);

}
