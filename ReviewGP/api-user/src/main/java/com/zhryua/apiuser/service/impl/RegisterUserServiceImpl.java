package com.zhryua.apiuser.service.impl;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import com.zhryua.apiuser.request.RegisterUserRequest;
import com.zhryua.apiuser.service.IRegisterUserService;
import com.zhryua.apiuser.service.IServiceUserRestTemplate;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

public class RegisterUserServiceImpl implements IRegisterUserService {

    @Autowired
    private IServiceUserRestTemplate iServiceUserRestTemplate;

    @Override
    public ResponseResult registerUser(RegisterUserRequest request) {

        JSONObject object = JSONObject.fromObject(request);
        InsertUserRequest insertUserRequest = (InsertUserRequest) JSONObject.toBean(object, InsertUserRequest.class);

        ResponseResult result = iServiceUserRestTemplate.insertUser(insertUserRequest);

        if(Objects.nonNull(result)) {

        }

        return null;
    }
}
