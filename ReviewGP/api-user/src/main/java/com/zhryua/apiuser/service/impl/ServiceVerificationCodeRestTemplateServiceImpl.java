package com.zhryua.apiuser.service.impl;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceverificationcode.request.VerifyCodeRequest;
import com.zhryua.apiuser.service.IServiceVerificationCodeRestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ServiceVerificationCodeRestTemplateServiceImpl implements IServiceVerificationCodeRestTemplateService {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public ResponseResult generatorCode(int identity, String phone) {
        String url = "http://service-verification-code/verify-code/generate/" + identity + "/" + phone;

        ResponseResult result = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(null, null), ResponseResult.class).getBody();

        return result;
    }

    @Override
    public ResponseResult verifyCode(int identity, String phone, String code) {
        String url = "http://service-verification-code/verify-code/verify/";

        VerifyCodeRequest request = new VerifyCodeRequest();
        request.setIdentity(identity).setCode(code).setPhone(phone);

        ResponseResult result = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(null, null), ResponseResult.class).getBody();

        return result;
    }
}
