package com.zhryua.apiuser.service.impl;

import com.zhryua.allcommon.constant.CommonStatusEnum;
import com.zhryua.allcommon.constant.IdentityConstant;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.apiuser.service.IAuthService;
import com.zhryua.apiuser.service.IServiceUserRestTemplate;
import com.zhryua.apiuser.service.IServiceVerificationCodeRestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements IAuthService {

//    @Autowired
//    private IServiceVerificationCodeRestTemplateService serviceVerificationCodeRestTemplateService;

    @Autowired
    private IServiceUserRestTemplate serviceUserRestTemplate;

    @Override
    public ResponseResult auth(String phone, String code) {
//
//        ResponseResult result = serviceVerificationCodeRestTemplateService.verifyCode(IdentityConstant.STUDENT, phone, code);
//        if(result.getCode() == CommonStatusEnum.FAIL.getCode()) {
//            return ResponseResult.fail("出现错误");
//        }
//
//
//        ResponseResult result1 = serviceUserRestTemplate.login(phone);
//        if(result1.getCode() == CommonStatusEnum.FAIL.getCode()) {
//            return ResponseResult.fail("出现错误");
//        }

        return ResponseResult.success("成功");
    }
}
