package com.zhryua.apiuser.service.impl;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicesms.SmsTemplateDto;
import com.zhryua.allcommon.dto.servicesms.request.SmsRequest;
import com.zhryua.apiuser.service.IServiceSmsRestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class ServiceSmsRestTemplateServiceImpl implements IServiceSmsRestTemplateService {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public ResponseResult sendSms(String phone, String code) {

        String url = "http://service-sms/send/sms-template";

        SmsRequest request = new SmsRequest();

        String[] phones = new String[] {phone};
        request.setReceivers(phones);

        List<SmsTemplateDto> list = new ArrayList<>();
        SmsTemplateDto dto = new SmsTemplateDto();
        dto.setId("SMS_1234567xxx【模板号】");
        HashMap<String, Object> map = new HashMap<>();
        map.put("code", code);
        dto.setTemplateMap(map);
        list.add(dto);

        request.setData(list);

        // restTemplate远程调用
        ResponseResult result = restTemplate.postForEntity(url, request, ResponseResult.class).getBody();

        return result;
    }
}
