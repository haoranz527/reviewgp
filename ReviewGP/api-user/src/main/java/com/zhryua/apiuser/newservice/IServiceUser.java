package com.zhryua.apiuser.newservice;

import com.zhryua.allcommon.api.serviceuser.IUserService;
import com.zhryua.allcommon.fallbackFactory.serviceuser.IServiceUserFallBackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

@FeignClient(name = "service-user", fallbackFactory = IServiceUserFallBackFactory.class)
@Service
public interface IServiceUser extends IUserService {
}
