package com.zhryua.apiuser.service;

import com.zhryua.allcommon.api.servicesms.IServiceSms;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "service-sms")
public interface IServiceSmsRestTemplateService extends IServiceSms {

}
