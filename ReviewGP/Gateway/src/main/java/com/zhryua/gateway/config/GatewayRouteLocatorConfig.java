package com.zhryua.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayRouteLocatorConfig {

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        RouteLocatorBuilder.Builder routes = builder.routes();

        for (String path : RouteServiceID.service_path) {
            routes.route(path,r->r.path(String.format("/pub/v1/%s/**",path))
                    .uri(String.format("lb://%s",path))).build();
        }

        return routes.build();

    }
}
