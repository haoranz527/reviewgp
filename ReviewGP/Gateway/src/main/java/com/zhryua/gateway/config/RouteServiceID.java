package com.zhryua.gateway.config;

import com.google.common.collect.Lists;

import java.util.List;

public interface RouteServiceID {

    List<String> service_path = Lists.newArrayList(
            "api-admin",
            "api-user"
            );

}
