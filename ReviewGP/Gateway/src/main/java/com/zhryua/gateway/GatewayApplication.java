package com.zhryua.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }


//    @Bean
//    public RouteLocator MainRouteLocator(RouteLocatorBuilder builder) {
//        return builder.routes()
//                .route("api-user",r->r.path("/user")
//                        .uri("http://localhost:8090"))
//                .route("api-admin", r->r.path("/admin")
//                        .uri("http://localhost:9012"))
//                .build();
//    }

    @PostMapping("/v1/hello")
    public String hello() {
        return "Hello";
    }

}
