package com.zhryua.gateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;


@Slf4j
public class MainGateway implements GlobalFilter, Ordered {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("进入filter");

        ServerHttpRequest request = exchange.getRequest();
        List<String> strings = request.getHeaders().get("token");
        if(!strings.isEmpty()) {
            String token = strings.get(0);
            String userId = (String) redisTemplate.opsForValue().get(token);
            // 在内存中拿到userid
            // 非空时
            if(StringUtils.isNotBlank(userId)) {
                return chain.filter(exchange);
            } else {
                log.error("拦截器出现问题");
                // 可设置跳转页面
                exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
                exchange.getResponse().setComplete();
            }
        }


        return null;
    }

    @Override
    public int getOrder() {
        // 数字越小越优先
        return 0;
    }
}
