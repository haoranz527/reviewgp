package com.zhryua.serviceuser;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import com.zhryua.serviceuser.service.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MainTest_1 {

    @Autowired
    private UserServiceImpl service;

    @Test
    public void insertUserTest() {
        InsertUserRequest request = new InsertUserRequest();

        request.setBed((short)2)
                .setAuditorId("211")
                .setPassword("1234567");
        ResponseResult result = service.insertUser(request);

        String s = result.getData().toString();

        System.out.println(s);

    }

    @Test
    public void loginTest(){
        String userid = "USER1510936000250105858";
        String password = "1234567";
        long time = 120;
        ResponseResult result = service.login(userid, password, time);
        System.out.println(result.getData().toString());
    }


}
