package com.zhryua.serviceuser.service;

import com.zhryua.allcommon.constant.CommonStatusEnum;
import com.zhryua.allcommon.constant.TimeConstant;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.SelectUserRequest;
import com.zhryua.allcommon.pojo.database.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class UserServiceImplTests {

    @Autowired
    private UserServiceImpl service;

    @Test
    public void selectUserTest() {
        SelectUserRequest request = new SelectUserRequest();
        List<String> ctm = new ArrayList<>();
        ctm.add("2022-03-10");
        ctm.add("2022-03-20");
        request.setCreateTime(ctm);
        ResponseResult result = service.selectUser(request);

        if (result.getCode() == CommonStatusEnum.SUCCESS.getCode()) {
            System.out.println("成功");
            List<User> list = (List<User>) result.getData();
            System.out.println(list.size());
            for (User user : list) {
                System.out.println(user.toString());
            }
        } else {
            System.out.println("失败");
        }
    }

    @Test
    public void login_test_1() {
        String userid = "USER1525506869433491458";
        String password = "123456";

        ResponseResult result = service.login(userid, password, TimeConstant.ONE_MONTH);

        if (result.getCode() == CommonStatusEnum.SUCCESS.getCode()) {
            System.out.println("OOOKKKKK");
        }
    }
}
