package com.zhryua.serviceuser.mapper;

import com.zhryua.allcommon.pojo.database.Card;
import com.zhryua.allcommon.pojo.database.CardExample;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.ibatis.annotations.Mapper;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Repository;

/**
 * CardMapper继承基类
 */
@Mapper
@Repository
public interface CardMapper extends MyBatisBaseDao<Card, String, CardExample> {
}