package com.zhryua.serviceuser.service;

import com.zhryua.allcommon.constant.IdPreConstant;
import com.zhryua.allcommon.constant.IntegerConstant;
import com.zhryua.allcommon.constant.ResponseConstantEnum;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.SelectUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.UpdateUserRequest;
import com.zhryua.allcommon.pojo.database.User;
import com.zhryua.allcommon.pojo.database.UserExample;
import com.zhryua.allcommon.util.IdWorker;
import com.zhryua.allcommon.util.MD5Util;
import com.zhryua.allcommon.util.TokenUtils;
import com.zhryua.serviceuser.mapper.UserMapper;
import com.zhryua.serviceuser.util.DateConveter;
import com.zhryua.serviceuser.util.RedisUtil;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class UserServiceImpl {

    @Autowired
    private UserMapper userMapper;

//    @Autowired
//    private RedisTemplate redisTemplate;

    @Autowired
    private RedisUtil redisUtil;

    private static IdWorker idWorker = new IdWorker();


    public ResponseResult login(String userid, String password, long time) {
        if (!StringUtils.isNotBlank(userid) || !StringUtils.isNotBlank(password)) {
            return ResponseResult.fail("不允许空值");
        }
        try {
            UserExample userExample = new UserExample();
            UserExample.Criteria criteria = userExample.createCriteria();
            criteria.andPasswordEqualTo(MD5Util.MD5(password.trim()))
                    .andUserIdEqualTo(userid.trim());
            long count = userMapper.countByExample(userExample);
            if (count == IntegerConstant.ONE) {
                String token = TokenUtils.createToken(userid);
                redisUtil.set(token, userid);
                redisUtil.expire(token, time, TimeUnit.MICROSECONDS);
                return ResponseResult.success(token);
            }
            return ResponseResult.fail("内部错误");
        } catch (Exception e) {
            return ResponseResult.fail("捕获错误 :" + e);
        }

    }

    public ResponseResult insertUser(InsertUserRequest request) {

        JSONObject object = JSONObject.fromObject(request);
        User user = (User) JSONObject.toBean(object, User.class);

        try {
            user.setBizId(IdPreConstant.BIZID + idWorker.nextId())
                    .setCardId(IdPreConstant.CARDID + idWorker.nextId())
                    .setUserId(IdPreConstant.USERID + idWorker.nextId())
                    .setCtm(new Date())
                    .setUtm(new Date())
                    .setIs_authentication(IntegerConstant.ZERO);

            if (StringUtils.isNotBlank(request.getName())) {
                user.setName(request.getName().trim());
            }
            if (StringUtils.isNotBlank(request.getPhone())) {
                user.setPhone(request.getPhone().trim());
            }
            if (StringUtils.isNotBlank(request.getPassword())) {
                user.setPassword(MD5Util.MD5(request.getPassword()));
            }
            if (StringUtils.isNotBlank(request.getEmail())) {
                user.setEmail(request.getEmail().trim());
            }
            if (StringUtils.isNotBlank(request.getQq())) {
                user.setQq(request.getQq());
            }
            if (StringUtils.isNotBlank(request.getWechat())) {
                user.setWechat(request.getWechat().trim());
            }
            if (ObjectUtils.isNotEmpty(request.getDepartment())) {
                user.setDepartment(request.getDepartment());
            }
            if (ObjectUtils.isNotEmpty(request.getClassNumber())) {
                user.setClassNumber(request.getClassNumber());
            }
            if (StringUtils.isNotBlank(request.getIdentity())) {
                user.setIdentity(request.getIdentity());
            }
            if (ObjectUtils.isNotEmpty(request.getIs_authentication())) {
                user.setIs_authentication(request.getIs_authentication());
            }
            if (ObjectUtils.isNotEmpty(request.getStatus())) {
                user.setStatus(request.getStatus());
            }
            if (ObjectUtils.isNotEmpty(request.getSubApp())) {
                user.setSubApp(request.getSubApp());
            }
            if (StringUtils.isNotBlank(request.getDormitoryId())) {
                user.setDormitoryId(request.getDormitoryId());
            }
            if (ObjectUtils.isNotEmpty(request.getBed())) {
                user.setBed(request.getBed());
            }
            if (StringUtils.isNotBlank(request.getImage())) {
                user.setImage(request.getImage());
            }

            int insert = userMapper.insert(user);
            if (insert < 1) {
                return ResponseResult.fail("失败");
            }

            return ResponseResult.success(user);
        } catch (Exception e) {
            return ResponseResult.fail("捕获异常："+ e);
        }
    }

    public ResponseResult logOut(String token) {
        try{
            if(StringUtils.isNotBlank(token)) {
                redisUtil.delete(token);
                return ResponseResult.success(ResponseConstantEnum.SUCCESS);
            }
            return ResponseResult.fail(ResponseConstantEnum.INTERNALERROR);
        } catch (Exception e) {
            return ResponseResult.fail(e);
        }
    }

    public ResponseResult updateUser(UpdateUserRequest request) {
        if(ObjectUtils.isEmpty(request)) {
            return ResponseResult.fail(ResponseConstantEnum.FAIL);
        }
        try{// 用户端只可以修改头像，密码，邮箱，qq，微信
            User user = userMapper.selectByPrimaryKey(request.getUserId());

            if(StringUtils.isNotBlank(request.getImage())) {
                user.setImage(request.getImage().trim());
            }

            if(StringUtils.isNotBlank(request.getPassword())) {
                user.setPassword(MD5Util.MD5(request.getPassword().trim()));
            }

            if(StringUtils.isNotBlank(request.getEmail())) {
                user.setEmail(request.getEmail().trim());
            }

            if(StringUtils.isNotBlank(request.getQq())) {
                user.setQq(request.getQq().trim());
            }

            if(StringUtils.isNotBlank(request.getWechat())) {
                user.setWechat(request.getWechat().trim());
            }

            int i = userMapper.updateByPrimaryKey(user);
            if(i > IntegerConstant.ZERO) {
                return ResponseResult.success(ResponseConstantEnum.SUCCESS);
            }
            return ResponseResult.fail(ResponseConstantEnum.FAIL);
        } catch (Exception e) {
            return ResponseResult.fail(ResponseConstantEnum.INTERNALERROR);
        }
    }

    // 查询用户
    public ResponseResult selectUser(SelectUserRequest request) {
        if(Objects.isNull(request)) {
            return ResponseResult.fail("is null");
        }
        try{
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();


            // userid
            if(StringUtils.isNotBlank(request.getUserId())) {
                criteria.andUserIdEqualTo(request.getUserId());
            }

            // 班级
            if(ObjectUtils.isNotEmpty(request.getClassNumber())) {
                criteria.andClassNumberEqualTo(request.getClassNumber());
            }

            // 院系
            if(ObjectUtils.isNotEmpty(request.getDepartment())) {
                criteria.andDepartmentEqualTo(request.getDepartment());
            }

            // name
            if(StringUtils.isNotBlank(request.getName())) {
                criteria.andNameLike(request.getName());
            }

            // email
            if(StringUtils.isNotBlank(request.getEmail())) {
                criteria.andEmailEqualTo(request.getEmail());
            }
            // phone
            if(StringUtils.isNotBlank(request.getEmail())) {
                criteria.andPhoneEqualTo(request.getPhone());
            }

            // 身份证号
            if(StringUtils.isNotBlank(request.getIdentity())) {
                criteria.andIdentityEqualTo(request.getIdentity());
            }

            // 账号状态
            if(ObjectUtils.isNotEmpty(request.getStatus())) {
                criteria.andStatusEqualTo(request.getStatus());
            }

            // 宿舍号
            if(ObjectUtils.isNotEmpty(request.getDormitoryId())) {
                criteria.andDormitoryIdEqualTo(request.getDormitoryId());
            }

            // 创建时间范围
            if(!CollectionUtils.isEmpty(request.getCreateTime())) {
                //只有一个时间，默认为起点时间, 只有起点时间，从起点时间检索志最新
                if(request.getCreateTime().size() == IntegerConstant.ONE) {
                    Date date = DateConveter.convert(request.getCreateTime().get(0));
                    criteria.andCtmGreaterThanOrEqualTo(date);
                }

                // 两个时间，按照begin，end检索
                if(request.getCreateTime().size() == IntegerConstant.TWO) {
                    Date begin = DateConveter.convert(request.getCreateTime().get(0));
                    Date end = DateConveter.convert(request.getCreateTime().get(1));
                    criteria.andCtmBetween(begin, end);
                }
            }

            // 开始查找
            // 没有使用page插件
            if(ObjectUtils.isNotEmpty(request.getPageSize()) && ObjectUtils.isNotEmpty(request.getPageNumber())) {
                Integer limit = request.getPageSize();
                Long offset = Long.valueOf((request.getPageNumber() - 1) * request.getPageSize());
                example.setLimit(limit);
                example.setOffset(offset);
            }

            // 排序
//            if(StringUtils.isNotBlank(request.getOrderBy())) {
//                example.setOrderByClause(request.getOrderBy());
//            }

            List<User> userList = userMapper.selectByExample(example);

            if(!CollectionUtils.isEmpty(userList)) {
                return ResponseResult.success(userList);
            }

            return ResponseResult.fail("内部错误");
        } catch (Exception e) {
            return ResponseResult.fail("内部错误" + e);
        }
    }

}
