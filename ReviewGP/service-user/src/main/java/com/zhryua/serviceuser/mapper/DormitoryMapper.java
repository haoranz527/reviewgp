package com.zhryua.serviceuser.mapper;

import com.zhryua.allcommon.pojo.database.Dormitory;
import com.zhryua.allcommon.pojo.database.DormitoryExample;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.ibatis.annotations.Mapper;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Repository;

/**
 * DormitoryMapper继承基类
 */
@Mapper
@Repository
public interface DormitoryMapper extends MyBatisBaseDao<Dormitory, String, DormitoryExample> {
}