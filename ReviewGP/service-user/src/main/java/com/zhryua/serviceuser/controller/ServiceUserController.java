package com.zhryua.serviceuser.controller;

import com.zhryua.allcommon.api.serviceuser.IUserService;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.SelectUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.UpdateUserRequest;
import com.zhryua.serviceuser.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceUserController implements IUserService {

    @Autowired
    private UserServiceImpl userService;

    @Override
    public ResponseResult login(String userid, String password, long time) {
        return userService.login(userid, password, time);
    }

    @Override
    public ResponseResult insertUser(InsertUserRequest request) {
        return userService.insertUser(request);
    }

    @Override
    public ResponseResult logOut(String token) {
        return userService.logOut(token);
    }

    @Override
    public ResponseResult updateUser(UpdateUserRequest request) {
        return userService.updateUser(request);
    }

    @Override
    public ResponseResult selectUser(SelectUserRequest request) {
        return userService.selectUser(request);
    }
}
