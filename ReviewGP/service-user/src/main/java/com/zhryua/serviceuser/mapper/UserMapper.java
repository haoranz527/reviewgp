package com.zhryua.serviceuser.mapper;

import com.zhryua.allcommon.pojo.database.User;
import com.zhryua.allcommon.pojo.database.UserExample;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.ibatis.annotations.Mapper;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Repository;

/**
 * UserMapper继承基类
 */
@Mapper
@Repository
public interface UserMapper extends MyBatisBaseDao<User, String, UserExample> {
}