package com.zhryua.serviceuser.controller;

import com.zhryua.allcommon.api.ITestApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestApiController implements ITestApi {

    @Value("${server.port}")
    private Integer port;

    @Override
    public String getHello() {
        return "Hello ,I'm from " + port;
    }

}
