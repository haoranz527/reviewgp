package com.zhryua.apiadmin.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pub/v1/api-admin")
public class AdminController {

    @GetMapping("/hello/aa")
    public String hellozuul() {
        return "Hello aa !";
    }

}
