package com.zhryua.serviceverificationcode.controller;

import com.zhryua.allcommon.api.serviceverificationcode.IVerificationCodeService;
import com.zhryua.allcommon.util.RandomCode;
import com.zhryua.serviceverificationcode.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
@SuppressWarnings("all")
public class VerifyCodeController implements IVerificationCodeService {

    @Autowired
    private RedisUtil redisUtil;


//    @Autowired
//    private IVerifyCodeService verifyCodeService;

//    @GetMapping("/generate/{identity}/{phone}")
//    public ResponseResult generate(@PathVariable("identity") int identity
//            , @PathVariable("phone") String phone) {
//
//        log.info("/generate/{identity}/{phone} ： 身份类型："+identity+",手机号：" + phone);
//        return verifyCodeService.generete(identity, phone);
//    }
//
//    @PostMapping("/verify")
//    public ResponseResult verify(@RequestBody VerifyCodeRequest request) {
//        String phone = request.getPhone();
//        int identity = request.getIdentity();
//        String code = request.getCode();
//        return verifyCodeService.verify(identity, phone, code);
//    }
//
//    @Override
//    public ResponseResult generatorCode(int identity, String phone) {
//        return verifyCodeService.generete(identity, phone);
//    }

//    @Override
//    public ResponseResult verifyCode(int identity, String phone, String code) {
//        return verifyCodeService.verify(identity, phone, code);
//    }

    @Override
    public String getCodeByPhone(String phone) {
        log.info("getCodeByPhone");
        String code = RandomCode.getCode();
        redisUtil.set(phone.trim(), code.trim());
        redisUtil.expire(phone, 2, TimeUnit.MINUTES);
        return code;
    }

    @Override
    public String getCodeByEmail(String email) {
        log.info("getCodeByEmail");
        String code = RandomCode.getCode();
        redisUtil.set(email.trim(), code.trim());
        redisUtil.expire(email, 2, TimeUnit.MINUTES);
        return code;
    }

    @Override
    public boolean verCode(String path, String code) {
        String s = redisUtil.get(path.trim());
        if (s.equals(code)) {
            return true;
        }
        return false;
    }
}
