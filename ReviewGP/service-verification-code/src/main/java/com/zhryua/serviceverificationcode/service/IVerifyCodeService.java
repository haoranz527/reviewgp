package com.zhryua.serviceverificationcode.service;

import com.zhryua.allcommon.dto.ResponseResult;

public interface IVerifyCodeService {

    public ResponseResult generete(int identity, String phone);

    public ResponseResult verify(int identity, String phone, String code);

}
