package com.zhryua.serviceverificationcode.service.impl;

import com.zhryua.allcommon.constant.IdentityConstant;
import com.zhryua.allcommon.constant.RedisKeyPrefixConstant;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceverificationcode.response.VerifyCodeResponse;
import com.zhryua.serviceverificationcode.service.IVerifyCodeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class VerifyCodeServiceImpl implements IVerifyCodeService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public ResponseResult generete(int identity, String phone) {
        // 生成验证码
        String code = String.valueOf((int) ((Math.random() * 9 + 1) * Math.pow(10, 5)));

        String keyPre = generateKeyPreByIdentity(identity);
        String key = keyPre + phone;
        // 存redis，2分钟过期
        BoundValueOperations<String, String> codeRedis = redisTemplate.boundValueOps(key);
        codeRedis.set(code, 30, TimeUnit.MINUTES);

        VerifyCodeResponse result = new VerifyCodeResponse();
        result.setCode(code);
        return ResponseResult.success(result);
    }

    @Override
    public ResponseResult verify(int identity, String phone, String code) {
        // 三挡验证校验，暂不做

        String keyPre = generateKeyPreByIdentity(identity);
        String key = keyPre + phone;

        BoundValueOperations<String, String> codeRedis = redisTemplate.boundValueOps(key);
        String redisCode = codeRedis.get();

        if(StringUtils.isNotBlank(code)
                && StringUtils.isNotBlank(redisCode)
                && code.trim().equals(redisCode.trim())) {
            log.info("验证成功");
            return ResponseResult.success("成功");
        } else {
            return ResponseResult.fail("验证失败");
        }
    }

    private String generateKeyPreByIdentity(int identity) {
        return identity == IdentityConstant.STUDENT ? RedisKeyPrefixConstant.STUDENT_LOGIN_CODE_KEY_PRE
                : RedisKeyPrefixConstant.TEACHER_LOGIN_CODE_KEY_PRE;
    }
}
