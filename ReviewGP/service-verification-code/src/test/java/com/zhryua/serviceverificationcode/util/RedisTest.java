package com.zhryua.serviceverificationcode.util;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;

@SpringBootTest
public class RedisTest {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    RedisUtil redisUtil;

    @Test
    public void setEmail_test_1() {
        redisUtil.setRedisTemplate(redisTemplate);

        redisUtil.set("zhryua","haohaohao");

        redisUtil.expire("zhryua", 1, TimeUnit.MINUTES);

        String s = redisUtil.get("zhryua");

        System.out.println(s);

    }

}
