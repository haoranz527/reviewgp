package com.zhryua.serviceverificationcode.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class VerifyCodeControllerTest{

    @Autowired
    private VerifyCodeController controller;

    @Test
    public void getCodeByPhone_test_1() {
        controller.getCodeByPhone("12311221");
    }

    @Test
    public void getCodeByEmail_test_1() {
        controller.getCodeByEmail("qwqwqqq@qq.com");
    }

}
