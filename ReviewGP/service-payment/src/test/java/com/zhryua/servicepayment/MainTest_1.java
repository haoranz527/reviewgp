package com.zhryua.servicepayment;

import com.zhryua.allcommon.constant.IdPreConstant;
import com.zhryua.allcommon.constant.PaymentTypeConstant;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicepayment.request.PayRequest;
import com.zhryua.allcommon.pojo.database.Card;
import com.zhryua.allcommon.util.IdWorker;
import com.zhryua.servicepayment.mapper.CardMapper;
import com.zhryua.servicepayment.service.PaymentServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MainTest_1 {

    @Autowired
    private PaymentServiceImpl service;

    @Autowired
    private CardMapper cardMapper;

    private IdWorker idWorker = new IdWorker();

    @Test
    public void MoneyPay_Test() {
        PayRequest request = new PayRequest();
        request.setAccount(IdPreConstant.USERID + "123")
                .setAmount(20000l)
                .setType(PaymentTypeConstant.MONEY);

        ResponseResult result = service.MoneyPay(request);

        System.out.println(result.getMessage());
    }

    @Test
    public void cardMapper_Test() {
        Card card = new Card();

        for (int i = 0; i < 1000000; i++) {
            card.setBizId(String.valueOf(idWorker.nextId()));
            card.setBalance(276666600l);
            card.setUserId(IdPreConstant.USERID+idWorker.nextId());
            cardMapper.insert(card);
        }

    }
}
