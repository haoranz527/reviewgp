package com.zhryua.servicepayment.mapper;

import com.zhryua.allcommon.pojo.database.TradeHistory;
import com.zhryua.allcommon.pojo.database.TradeHistoryExample;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.ibatis.annotations.Mapper;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Repository;

/**
 * TradeHistoryMapper继承基类
 */
@Mapper
@Repository
public interface TradeHistoryMapper extends MyBatisBaseDao<TradeHistory, String, TradeHistoryExample> {
}