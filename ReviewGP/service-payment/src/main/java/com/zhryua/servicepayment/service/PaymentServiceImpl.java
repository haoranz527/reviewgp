package com.zhryua.servicepayment.service;

import com.zhryua.allcommon.api.servicepayment.IPaymentApi;
import com.zhryua.allcommon.constant.CommonStatusEnum;
import com.zhryua.allcommon.constant.IdPreConstant;
import com.zhryua.allcommon.constant.IntegerConstant;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicepayment.request.PayRequest;
import com.zhryua.allcommon.pojo.database.Card;
import com.zhryua.allcommon.pojo.database.CardExample;
import com.zhryua.allcommon.pojo.database.TradeHistory;
import com.zhryua.allcommon.util.IdWorker;
import com.zhryua.servicepayment.mapper.CardMapper;
import com.zhryua.servicepayment.mapper.TradeHistoryMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PaymentServiceImpl implements IPaymentApi {

    private static final IdWorker idWorker = new IdWorker();

    @Autowired
    private TradeHistoryMapper tradeHistoryMapper;

    @Autowired
    private CardMapper cardMapper;


    @Override
    public ResponseResult AliPay(PayRequest request) {
        boolean b = true;// 调用支付宝api
        if(!b) {
            return ResponseResult.success(CommonStatusEnum.FAIL);
        }
        return ResponseResult.success(CommonStatusEnum.SUCCESS);
    }

    @Override
    public ResponseResult WechatPay(PayRequest request) {
        boolean b = true;// 调用支付宝api
        if(!b) {
            return ResponseResult.fail(CommonStatusEnum.FAIL);
        }
        return ResponseResult.success(CommonStatusEnum.SUCCESS);
    }

    @Override
    public ResponseResult MoneyPay(PayRequest request) {

        if(request == null) {
            return ResponseResult.fail(CommonStatusEnum.FAIL);
        }

        try{
            CardExample cardExample = new CardExample();
            CardExample.Criteria criteria = cardExample.createCriteria();
            if(StringUtils.isNotBlank(request.getAccount())) {
                criteria.andUserIdEqualTo(request.getAccount());
            }
            // 1 为正常使用状态
            criteria.andStatusEqualTo(IntegerConstant.ONE);
            List<Card> cardList = cardMapper.selectByExample(cardExample);
            if(cardList.size() == IntegerConstant.ZERO || cardList.size() > IntegerConstant.ONE) {
                return ResponseResult.fail("错误");
            }
            Long amount = cardList.get(0).getBalance() + request.getAmount();
            cardList.get(0).setBalance(amount);
            int update = cardMapper.updateByPrimaryKey(cardList.get(0));
            if(update == IntegerConstant.ZERO) {
                return ResponseResult.fail("错误");
            }

            TradeHistory tradeHistory = new TradeHistory();
            tradeHistory.setBizId(IdPreConstant.BIZID + idWorker.nextId())
                    .setAmount(request.getAmount())
                    .setItem("现金缴费")
                    .setEtm(new Date())
                    .setConsumption(IntegerConstant.TWO)
                    .setStatus(IntegerConstant.ONE)
                    .setAddress("线下现金充值");
            int insert = tradeHistoryMapper.insert(tradeHistory);
            if(insert < IntegerConstant.ONE) {
                return ResponseResult.fail("错误");
            }

            // 写入数据库成功后，强行将新充值的金额写入到redis中
            // TODO


            return ResponseResult.success("成功");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.fail(e);
        }
    }
}
