package com.zhryua.serviceconsume.controller;

import com.zhryua.allcommon.api.serviceconsume.IConsumeService;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceconsume.request.ConsumRequest;
import com.zhryua.serviceconsume.service.ConsumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumeController implements IConsumeService {

    @Autowired
    private ConsumeService service;

    @Override
    public ResponseResult debit(ConsumRequest request) {
        return service.debit(request);
    }
}
