package com.zhryua.serviceconsume.service;

import com.alibaba.fastjson2.JSON;
import com.zhryua.allcommon.constant.RedisConstant;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicecard.request.SelectCardRequest;
import com.zhryua.allcommon.dto.serviceconsume.request.ConsumRequest;
import com.zhryua.allcommon.pojo.database.Card;
import com.zhryua.serviceconsume.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@SuppressWarnings("all")
public class ConsumeService {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private IServiceCard cardService;

    // 实现消费扣款
    public ResponseResult debit(ConsumRequest request){
        if(null == request) {
            return ResponseResult.fail("request为空");
        }
        try{
            // 交易金额
            Long amount = request.getAmount();
            // 用户id
            String userId = request.getUserId();

            // 检查用户的卡余额是否在redis中，如果不在则导入
            long balance = checkRedis(userId) - amount;
            if(balance >= 0) {
                redisUtil.set(RedisConstant.CARDCONSUME+userId, Long.toString(balance));
                return ResponseResult.success("success");
            } else {
                return ResponseResult.fail("余额不足");
            }
        } catch (Exception e) {
            log.error("发生未知系统错误");
            return ResponseResult.fail(e);
        }

    }

    // 检查用户的卡余额是否在redis中，如果不在则导入.返回余额
    public Long checkRedis(String userid) {

        String key = RedisConstant.CARDCONSUME+userid;
        Boolean b = redisUtil.hasKey(key);
        if(!b){// 不存在
            try{
                ResponseResult result = cardService.selectCard(new SelectCardRequest().setUserId(userid));
                String jsonString = JSON.toJSONString(result.getData());
                List<Card> cardList = JSON.parseArray(jsonString, Card.class);
                Card card = cardList.get(0);
                Long cardBalance = card.getBalance();
                // 保存到redis
                redisUtil.set(RedisConstant.CARDCONSUME+userid, cardBalance.toString());
                return cardBalance;
            } catch (Exception e) {
                log.error("check redis 发生未知错误");
                e.printStackTrace();
                return -1L;
            }
        } else {
            String s = redisUtil.get(RedisConstant.CARDCONSUME + userid);
            return Long.valueOf(s);
        }
    }


}
