package com.zhryua.servicecard.util;




import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConveter{

    public static Date convert(String dateStr) {

        try {
            if(dateStr != null && dateStr != ""){
                // 把字符串转换为日期类型
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
                return simpleDateFormat.parse(dateStr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void main(String[] args) {
        String date = "2022-04-15 01:22:43";
        Date convert = convert(date);
        System.out.println(convert);
    }

}
