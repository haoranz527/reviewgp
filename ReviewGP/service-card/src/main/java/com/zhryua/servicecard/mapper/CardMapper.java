package com.zhryua.servicecard.mapper;

import com.zhryua.allcommon.pojo.database.Card;
import com.zhryua.allcommon.pojo.database.CardExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * CardMapper继承基类
 */
@Mapper
@Repository
public interface CardMapper extends MyBatisBaseDao<Card, String, CardExample> {
}