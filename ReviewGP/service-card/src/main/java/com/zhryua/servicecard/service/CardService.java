package com.zhryua.servicecard.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhryua.allcommon.constant.IdPreConstant;
import com.zhryua.allcommon.dto.servicecard.request.SelectCardRequest;
import com.zhryua.allcommon.pojo.database.CardExample;
import com.zhryua.servicecard.mapper.CardMapper;
import com.zhryua.allcommon.constant.IntegerConstant;
import com.zhryua.allcommon.constant.ResponseConstantEnum;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicecard.request.InsertCardRequest;
import com.zhryua.allcommon.dto.servicecard.request.UpdateCardRequest;
import com.zhryua.allcommon.pojo.database.Card;
import com.zhryua.allcommon.util.IdWorker;
import com.zhryua.servicecard.util.DateConveter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.util.*;

@Service
public class CardService {

    private static IdWorker idWorker = new IdWorker();

    @Autowired
    private CardMapper cardMapper;

    public ResponseResult insertCard(InsertCardRequest request) {
        if (Objects.isNull(request)) {
            return ResponseResult.fail(ResponseConstantEnum.FAIL);
        }

        try {
            Card card = new Card();
            card.setStatus(0)
                    .setMtm(new Date())
                    .setAtm(new Date());
            if (StringUtils.isNotBlank(request.getBizId())) {
                card.setBizId(request.getBizId());
            } else {
                card.setBizId(IdPreConstant.CARDID + idWorker.nextId());
            }

            if (StringUtils.isNotBlank(request.getUserId())) {
                card.setUserId(request.getUserId().trim());
            }

            if (!Objects.isNull(request.getLimit())) {
                card.setLimit(request.getLimit());
            }

            if (!Objects.isNull(request.getBalance())) {
                card.setBalance(request.getBalance());
            }

            int i = cardMapper.insert(card);
            if (i != IntegerConstant.ONE) {
                return ResponseResult.fail("内部错误");
            }
            return ResponseResult.success(card);
        } catch (Exception e) {
            return ResponseResult.fail("内部错误" + e);
        }
    }

    public ResponseResult UpdateCard(UpdateCardRequest request) {
        if (Objects.isNull(request)) {
            return ResponseResult.fail("is NULL");
        }

        try {
            CardExample cardExample = new CardExample();
            CardExample.Criteria criteria = cardExample.createCriteria();
            criteria.andStatusEqualTo(IntegerConstant.ZERO)
                    .andUserIdEqualTo(request.getUserId());

            List<Card> cards = cardMapper.selectByExample(cardExample);

            if (cards.size() > IntegerConstant.ONE) {
                return ResponseResult.fail("内部错误");
            } else {

                Card card = cards.get(0);

                if (!Objects.isNull(request.getStatus())) {
                    card.setStatus(request.getStatus());
                }

                if (!Objects.isNull(request.getLimit())) {
                    card.setLimit(request.getLimit());
                }

                if (!Objects.isNull(request.getBalance())) {
                    card.setBalance(request.getBalance());
                }

                card.setAtm(new Date());

                int i = cardMapper.updateByPrimaryKey(card);

                if (i == IntegerConstant.ONE) {
                    return ResponseResult.success(card);
                }

            }

            return ResponseResult.fail("失败");

        } catch (Exception e) {

            return ResponseResult.fail("内部错误" + e);

        }
    }


    public ResponseResult selectCard(SelectCardRequest request) {

        if (Objects.isNull(request)) {
            return ResponseResult.fail("is null");
        }

        try {
            CardExample cardExample = new CardExample();
            CardExample.Criteria criteria = cardExample.createCriteria();

            if (!CollectionUtils.isEmpty(request.getBalances())) {
                criteria.andBalanceBetween(request.getBalances().get(0), request.getBalances().get(1));
            }

            if (StringUtils.isNotBlank(request.getUserId())) {
                criteria.andUserIdEqualTo(request.getUserId());
            }

            if (!CollectionUtils.isEmpty(request.getCreatTime())) {
                Date begin = DateConveter.convert(request.getCreatTime().get(0));
                Date end = DateConveter.convert(request.getCreatTime().get(1));
                criteria.andMtmBetween(begin, end);
            }

            if (!CollectionUtils.isEmpty(request.getUpdateTime())) {
                Date begin = DateConveter.convert(request.getUpdateTime().get(0));
                Date end = DateConveter.convert(request.getUpdateTime().get(1));
                criteria.andAtmBetween(begin, end);
            }

            if (!CollectionUtils.isEmpty(request.getLimits())) {
                criteria.andLimitBetween(request.getLimits().get(0), request.getLimits().get(1));
            }

            PageHelper.startPage(request.getPageNumber(), request.getPageSize());
            List<Card> cardList = cardMapper.selectByExample(cardExample);

            if (!CollectionUtils.isEmpty(cardList)) {
                PageInfo<Card> pageInfo = new PageInfo<>(cardList);
                return ResponseResult.success(pageInfo.getList());
            }

            return ResponseResult.fail("内部错误");
        } catch (Exception e) {

            return ResponseResult.fail("内部错误" + e);

        }

    }
}
