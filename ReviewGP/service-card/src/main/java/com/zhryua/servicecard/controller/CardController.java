package com.zhryua.servicecard.controller;

import com.zhryua.allcommon.api.servicecard.ICardService;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicecard.request.InsertCardRequest;
import com.zhryua.allcommon.dto.servicecard.request.SelectCardRequest;
import com.zhryua.allcommon.dto.servicecard.request.UpdateCardRequest;
import com.zhryua.servicecard.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CardController implements ICardService {

    @Autowired
    private CardService cardService;

    @Override
    public ResponseResult insertCard(InsertCardRequest request) {
        return cardService.insertCard(request);
    }

    @Override
    public ResponseResult UpdateCard(UpdateCardRequest request) {
        return cardService.UpdateCard(request);
    }

    @Override
    public ResponseResult selectCard(SelectCardRequest request) {
        return cardService.selectCard(request);
    }

}
