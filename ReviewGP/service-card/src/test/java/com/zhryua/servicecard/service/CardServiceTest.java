package com.zhryua.servicecard.service;

import com.zhryua.allcommon.constant.CommonStatusEnum;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicecard.request.InsertCardRequest;
import com.zhryua.allcommon.dto.servicecard.request.SelectCardRequest;
import com.zhryua.allcommon.dto.servicecard.request.UpdateCardRequest;
import com.zhryua.allcommon.pojo.database.Card;
import com.zhryua.allcommon.util.IdWorker;
import lombok.Data;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CardServiceTest {

    @Autowired
    private CardService service;

    private static IdWorker idWorker = new IdWorker();

    @Test
    public void insertCardTest_1() {
        InsertCardRequest request = new InsertCardRequest();

        for (int i = 0; i < 100; i++) {
            request.setUserId(idWorker.nextId() + "")
                    .setLimit(2761L)
                    .setBalance(5254165L);

            ResponseResult result = service.insertCard(request);

            if(result.getCode() == CommonStatusEnum.SUCCESS.getCode()) {
                System.out.println("成功");
            } else {
                System.out.println("失败" + result.getData());
            }

        }

    }


    @Test
    public void UpdateCardTest() {
        UpdateCardRequest request = new UpdateCardRequest();

        request.setUserId("hshahgdhas")
                .setStatus(3);

        ResponseResult result = service.UpdateCard(request);

        if(result.getCode() == CommonStatusEnum.SUCCESS.getCode()) {
            System.out.println("成功");
        } else {
            System.out.println("失败");
        }

    }

    @Test
    public void selectCardTest() {
        SelectCardRequest request = new SelectCardRequest();
        request.setPageNumber(20).setPageSize(3);

//        request.setUserId("1514989561636315136");

        ResponseResult result = service.selectCard(request);

        if(result.getCode() == CommonStatusEnum.SUCCESS.getCode()) {
            List<Card> data = (List<Card>) (result.getData());
            System.out.println("成功");
            System.out.println("pagesize: " + data.size());
            System.out.println(data.toString());
        } else {
            System.out.println("失败");
        }
    }

    @Test
    public void test() {
        Type type = new Type();
        type.setA(new int[3]);
        type.getA()[0] = 10;
        type.getA()[1] = 11;
        type.getA()[2] = 12;

        int length = type.getA().length;

        for (int i = 0; i < length; i++) {
            System.out.println(type.getA()[i]);
        }

    }

    @Data
    private class Type {
        private int[] a;
    }
}
