package com.zhryua.allcommon.dto.servicesms;


import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@Accessors(chain = true)
public class SmsTemplateDto {


    private String id;

    private Map<String, Object> templateMap;
}
