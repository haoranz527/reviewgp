package com.zhryua.allcommon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AllCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(AllCommonApplication.class, args);
    }

}
