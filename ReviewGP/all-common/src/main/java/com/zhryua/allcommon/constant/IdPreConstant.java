package com.zhryua.allcommon.constant;
// 各个id字段的前缀
public class IdPreConstant {

    public static final String BIZID = "BIZ";

    public static final String USERID = "USER";

    public static final String CARDID = "CARD";

}
