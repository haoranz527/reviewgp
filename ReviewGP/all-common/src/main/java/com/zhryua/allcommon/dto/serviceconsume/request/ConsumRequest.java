package com.zhryua.allcommon.dto.serviceconsume.request;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class ConsumRequest {

    /**
     * 1-校园卡2-现金3-微信4-支付宝5-银联6-充值7-其他（1-5为交易形式，6为校园卡充值，7为预留位）
     * 默认为卡消费
     */
    @Value("1")
    private Integer consumption;

    /**
     * 交易金额
     */
    @NotNull
    private Long amount;

    /**
     * 交易地点
     */
    private String address;

    /**
     *  用户id
     */
    @NotNull
    private String userId;
}
