package com.zhryua.allcommon.pojo.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

public class DormitoryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public DormitoryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBizIdIsNull() {
            addCriterion("dormitory.bizId is null");
            return (Criteria) this;
        }

        public Criteria andBizIdIsNotNull() {
            addCriterion("dormitory.bizId is not null");
            return (Criteria) this;
        }

        public Criteria andBizIdEqualTo(String value) {
            addCriterion("dormitory.bizId =", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotEqualTo(String value) {
            addCriterion("dormitory.bizId <>", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdGreaterThan(String value) {
            addCriterion("dormitory.bizId >", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdGreaterThanOrEqualTo(String value) {
            addCriterion("dormitory.bizId >=", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdLessThan(String value) {
            addCriterion("dormitory.bizId <", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdLessThanOrEqualTo(String value) {
            addCriterion("dormitory.bizId <=", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdLike(String value) {
            addCriterion("dormitory.bizId like", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotLike(String value) {
            addCriterion("dormitory.bizId not like", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdIn(List<String> values) {
            addCriterion("dormitory.bizId in", values, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotIn(List<String> values) {
            addCriterion("dormitory.bizId not in", values, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdBetween(String value1, String value2) {
            addCriterion("dormitory.bizId between", value1, value2, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotBetween(String value1, String value2) {
            addCriterion("dormitory.bizId not between", value1, value2, "bizId");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("dormitory.`status` is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("dormitory.`status` is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Short value) {
            addCriterion("dormitory.`status` =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Short value) {
            addCriterion("dormitory.`status` <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Short value) {
            addCriterion("dormitory.`status` >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Short value) {
            addCriterion("dormitory.`status` >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Short value) {
            addCriterion("dormitory.`status` <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Short value) {
            addCriterion("dormitory.`status` <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Short> values) {
            addCriterion("dormitory.`status` in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Short> values) {
            addCriterion("dormitory.`status` not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Short value1, Short value2) {
            addCriterion("dormitory.`status` between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Short value1, Short value2) {
            addCriterion("dormitory.`status` not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andPeopleIsNull() {
            addCriterion("dormitory.people is null");
            return (Criteria) this;
        }

        public Criteria andPeopleIsNotNull() {
            addCriterion("dormitory.people is not null");
            return (Criteria) this;
        }

        public Criteria andPeopleEqualTo(Short value) {
            addCriterion("dormitory.people =", value, "people");
            return (Criteria) this;
        }

        public Criteria andPeopleNotEqualTo(Short value) {
            addCriterion("dormitory.people <>", value, "people");
            return (Criteria) this;
        }

        public Criteria andPeopleGreaterThan(Short value) {
            addCriterion("dormitory.people >", value, "people");
            return (Criteria) this;
        }

        public Criteria andPeopleGreaterThanOrEqualTo(Short value) {
            addCriterion("dormitory.people >=", value, "people");
            return (Criteria) this;
        }

        public Criteria andPeopleLessThan(Short value) {
            addCriterion("dormitory.people <", value, "people");
            return (Criteria) this;
        }

        public Criteria andPeopleLessThanOrEqualTo(Short value) {
            addCriterion("dormitory.people <=", value, "people");
            return (Criteria) this;
        }

        public Criteria andPeopleIn(List<Short> values) {
            addCriterion("dormitory.people in", values, "people");
            return (Criteria) this;
        }

        public Criteria andPeopleNotIn(List<Short> values) {
            addCriterion("dormitory.people not in", values, "people");
            return (Criteria) this;
        }

        public Criteria andPeopleBetween(Short value1, Short value2) {
            addCriterion("dormitory.people between", value1, value2, "people");
            return (Criteria) this;
        }

        public Criteria andPeopleNotBetween(Short value1, Short value2) {
            addCriterion("dormitory.people not between", value1, value2, "people");
            return (Criteria) this;
        }

        public Criteria andRepairsIsNull() {
            addCriterion("dormitory.repairs is null");
            return (Criteria) this;
        }

        public Criteria andRepairsIsNotNull() {
            addCriterion("dormitory.repairs is not null");
            return (Criteria) this;
        }

        public Criteria andRepairsEqualTo(Short value) {
            addCriterion("dormitory.repairs =", value, "repairs");
            return (Criteria) this;
        }

        public Criteria andRepairsNotEqualTo(Short value) {
            addCriterion("dormitory.repairs <>", value, "repairs");
            return (Criteria) this;
        }

        public Criteria andRepairsGreaterThan(Short value) {
            addCriterion("dormitory.repairs >", value, "repairs");
            return (Criteria) this;
        }

        public Criteria andRepairsGreaterThanOrEqualTo(Short value) {
            addCriterion("dormitory.repairs >=", value, "repairs");
            return (Criteria) this;
        }

        public Criteria andRepairsLessThan(Short value) {
            addCriterion("dormitory.repairs <", value, "repairs");
            return (Criteria) this;
        }

        public Criteria andRepairsLessThanOrEqualTo(Short value) {
            addCriterion("dormitory.repairs <=", value, "repairs");
            return (Criteria) this;
        }

        public Criteria andRepairsIn(List<Short> values) {
            addCriterion("dormitory.repairs in", values, "repairs");
            return (Criteria) this;
        }

        public Criteria andRepairsNotIn(List<Short> values) {
            addCriterion("dormitory.repairs not in", values, "repairs");
            return (Criteria) this;
        }

        public Criteria andRepairsBetween(Short value1, Short value2) {
            addCriterion("dormitory.repairs between", value1, value2, "repairs");
            return (Criteria) this;
        }

        public Criteria andRepairsNotBetween(Short value1, Short value2) {
            addCriterion("dormitory.repairs not between", value1, value2, "repairs");
            return (Criteria) this;
        }

        public Criteria andItemIsNull() {
            addCriterion("dormitory.item is null");
            return (Criteria) this;
        }

        public Criteria andItemIsNotNull() {
            addCriterion("dormitory.item is not null");
            return (Criteria) this;
        }

        public Criteria andItemEqualTo(String value) {
            addCriterion("dormitory.item =", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotEqualTo(String value) {
            addCriterion("dormitory.item <>", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemGreaterThan(String value) {
            addCriterion("dormitory.item >", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemGreaterThanOrEqualTo(String value) {
            addCriterion("dormitory.item >=", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemLessThan(String value) {
            addCriterion("dormitory.item <", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemLessThanOrEqualTo(String value) {
            addCriterion("dormitory.item <=", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemLike(String value) {
            addCriterion("dormitory.item like", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotLike(String value) {
            addCriterion("dormitory.item not like", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemIn(List<String> values) {
            addCriterion("dormitory.item in", values, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotIn(List<String> values) {
            addCriterion("dormitory.item not in", values, "item");
            return (Criteria) this;
        }

        public Criteria andItemBetween(String value1, String value2) {
            addCriterion("dormitory.item between", value1, value2, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotBetween(String value1, String value2) {
            addCriterion("dormitory.item not between", value1, value2, "item");
            return (Criteria) this;
        }

        public Criteria andCtmIsNull() {
            addCriterion("dormitory.ctm is null");
            return (Criteria) this;
        }

        public Criteria andCtmIsNotNull() {
            addCriterion("dormitory.ctm is not null");
            return (Criteria) this;
        }

        public Criteria andCtmEqualTo(Date value) {
            addCriterion("dormitory.ctm =", value, "ctm");
            return (Criteria) this;
        }

        public Criteria andCtmNotEqualTo(Date value) {
            addCriterion("dormitory.ctm <>", value, "ctm");
            return (Criteria) this;
        }

        public Criteria andCtmGreaterThan(Date value) {
            addCriterion("dormitory.ctm >", value, "ctm");
            return (Criteria) this;
        }

        public Criteria andCtmGreaterThanOrEqualTo(Date value) {
            addCriterion("dormitory.ctm >=", value, "ctm");
            return (Criteria) this;
        }

        public Criteria andCtmLessThan(Date value) {
            addCriterion("dormitory.ctm <", value, "ctm");
            return (Criteria) this;
        }

        public Criteria andCtmLessThanOrEqualTo(Date value) {
            addCriterion("dormitory.ctm <=", value, "ctm");
            return (Criteria) this;
        }

        public Criteria andCtmIn(List<Date> values) {
            addCriterion("dormitory.ctm in", values, "ctm");
            return (Criteria) this;
        }

        public Criteria andCtmNotIn(List<Date> values) {
            addCriterion("dormitory.ctm not in", values, "ctm");
            return (Criteria) this;
        }

        public Criteria andCtmBetween(Date value1, Date value2) {
            addCriterion("dormitory.ctm between", value1, value2, "ctm");
            return (Criteria) this;
        }

        public Criteria andCtmNotBetween(Date value1, Date value2) {
            addCriterion("dormitory.ctm not between", value1, value2, "ctm");
            return (Criteria) this;
        }

        public Criteria andUseTimeIsNull() {
            addCriterion("dormitory.useTime is null");
            return (Criteria) this;
        }

        public Criteria andUseTimeIsNotNull() {
            addCriterion("dormitory.useTime is not null");
            return (Criteria) this;
        }

        public Criteria andUseTimeEqualTo(Integer value) {
            addCriterion("dormitory.useTime =", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeNotEqualTo(Integer value) {
            addCriterion("dormitory.useTime <>", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeGreaterThan(Integer value) {
            addCriterion("dormitory.useTime >", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("dormitory.useTime >=", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeLessThan(Integer value) {
            addCriterion("dormitory.useTime <", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeLessThanOrEqualTo(Integer value) {
            addCriterion("dormitory.useTime <=", value, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeIn(List<Integer> values) {
            addCriterion("dormitory.useTime in", values, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeNotIn(List<Integer> values) {
            addCriterion("dormitory.useTime not in", values, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeBetween(Integer value1, Integer value2) {
            addCriterion("dormitory.useTime between", value1, value2, "useTime");
            return (Criteria) this;
        }

        public Criteria andUseTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("dormitory.useTime not between", value1, value2, "useTime");
            return (Criteria) this;
        }

        public Criteria andPowerIsNull() {
            addCriterion("dormitory.`power` is null");
            return (Criteria) this;
        }

        public Criteria andPowerIsNotNull() {
            addCriterion("dormitory.`power` is not null");
            return (Criteria) this;
        }

        public Criteria andPowerEqualTo(Long value) {
            addCriterion("dormitory.`power` =", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerNotEqualTo(Long value) {
            addCriterion("dormitory.`power` <>", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerGreaterThan(Long value) {
            addCriterion("dormitory.`power` >", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerGreaterThanOrEqualTo(Long value) {
            addCriterion("dormitory.`power` >=", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerLessThan(Long value) {
            addCriterion("dormitory.`power` <", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerLessThanOrEqualTo(Long value) {
            addCriterion("dormitory.`power` <=", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerIn(List<Long> values) {
            addCriterion("dormitory.`power` in", values, "power");
            return (Criteria) this;
        }

        public Criteria andPowerNotIn(List<Long> values) {
            addCriterion("dormitory.`power` not in", values, "power");
            return (Criteria) this;
        }

        public Criteria andPowerBetween(Long value1, Long value2) {
            addCriterion("dormitory.`power` between", value1, value2, "power");
            return (Criteria) this;
        }

        public Criteria andPowerNotBetween(Long value1, Long value2) {
            addCriterion("dormitory.`power` not between", value1, value2, "power");
            return (Criteria) this;
        }

        public Criteria andWaterIsNull() {
            addCriterion("dormitory.water is null");
            return (Criteria) this;
        }

        public Criteria andWaterIsNotNull() {
            addCriterion("dormitory.water is not null");
            return (Criteria) this;
        }

        public Criteria andWaterEqualTo(Long value) {
            addCriterion("dormitory.water =", value, "water");
            return (Criteria) this;
        }

        public Criteria andWaterNotEqualTo(Long value) {
            addCriterion("dormitory.water <>", value, "water");
            return (Criteria) this;
        }

        public Criteria andWaterGreaterThan(Long value) {
            addCriterion("dormitory.water >", value, "water");
            return (Criteria) this;
        }

        public Criteria andWaterGreaterThanOrEqualTo(Long value) {
            addCriterion("dormitory.water >=", value, "water");
            return (Criteria) this;
        }

        public Criteria andWaterLessThan(Long value) {
            addCriterion("dormitory.water <", value, "water");
            return (Criteria) this;
        }

        public Criteria andWaterLessThanOrEqualTo(Long value) {
            addCriterion("dormitory.water <=", value, "water");
            return (Criteria) this;
        }

        public Criteria andWaterIn(List<Long> values) {
            addCriterion("dormitory.water in", values, "water");
            return (Criteria) this;
        }

        public Criteria andWaterNotIn(List<Long> values) {
            addCriterion("dormitory.water not in", values, "water");
            return (Criteria) this;
        }

        public Criteria andWaterBetween(Long value1, Long value2) {
            addCriterion("dormitory.water between", value1, value2, "water");
            return (Criteria) this;
        }

        public Criteria andWaterNotBetween(Long value1, Long value2) {
            addCriterion("dormitory.water not between", value1, value2, "water");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdIsNull() {
            addCriterion("dormitory.principalId is null");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdIsNotNull() {
            addCriterion("dormitory.principalId is not null");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdEqualTo(String value) {
            addCriterion("dormitory.principalId =", value, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdNotEqualTo(String value) {
            addCriterion("dormitory.principalId <>", value, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdGreaterThan(String value) {
            addCriterion("dormitory.principalId >", value, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdGreaterThanOrEqualTo(String value) {
            addCriterion("dormitory.principalId >=", value, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdLessThan(String value) {
            addCriterion("dormitory.principalId <", value, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdLessThanOrEqualTo(String value) {
            addCriterion("dormitory.principalId <=", value, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdLike(String value) {
            addCriterion("dormitory.principalId like", value, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdNotLike(String value) {
            addCriterion("dormitory.principalId not like", value, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdIn(List<String> values) {
            addCriterion("dormitory.principalId in", values, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdNotIn(List<String> values) {
            addCriterion("dormitory.principalId not in", values, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdBetween(String value1, String value2) {
            addCriterion("dormitory.principalId between", value1, value2, "principalId");
            return (Criteria) this;
        }

        public Criteria andPrincipalIdNotBetween(String value1, String value2) {
            addCriterion("dormitory.principalId not between", value1, value2, "principalId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdIsNull() {
            addCriterion("dormitory.airConditionId is null");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdIsNotNull() {
            addCriterion("dormitory.airConditionId is not null");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdEqualTo(String value) {
            addCriterion("dormitory.airConditionId =", value, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdNotEqualTo(String value) {
            addCriterion("dormitory.airConditionId <>", value, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdGreaterThan(String value) {
            addCriterion("dormitory.airConditionId >", value, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdGreaterThanOrEqualTo(String value) {
            addCriterion("dormitory.airConditionId >=", value, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdLessThan(String value) {
            addCriterion("dormitory.airConditionId <", value, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdLessThanOrEqualTo(String value) {
            addCriterion("dormitory.airConditionId <=", value, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdLike(String value) {
            addCriterion("dormitory.airConditionId like", value, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdNotLike(String value) {
            addCriterion("dormitory.airConditionId not like", value, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdIn(List<String> values) {
            addCriterion("dormitory.airConditionId in", values, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdNotIn(List<String> values) {
            addCriterion("dormitory.airConditionId not in", values, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdBetween(String value1, String value2) {
            addCriterion("dormitory.airConditionId between", value1, value2, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andAirConditionIdNotBetween(String value1, String value2) {
            addCriterion("dormitory.airConditionId not between", value1, value2, "airConditionId");
            return (Criteria) this;
        }

        public Criteria andWaterIdIsNull() {
            addCriterion("dormitory.waterId is null");
            return (Criteria) this;
        }

        public Criteria andWaterIdIsNotNull() {
            addCriterion("dormitory.waterId is not null");
            return (Criteria) this;
        }

        public Criteria andWaterIdEqualTo(String value) {
            addCriterion("dormitory.waterId =", value, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdNotEqualTo(String value) {
            addCriterion("dormitory.waterId <>", value, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdGreaterThan(String value) {
            addCriterion("dormitory.waterId >", value, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdGreaterThanOrEqualTo(String value) {
            addCriterion("dormitory.waterId >=", value, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdLessThan(String value) {
            addCriterion("dormitory.waterId <", value, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdLessThanOrEqualTo(String value) {
            addCriterion("dormitory.waterId <=", value, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdLike(String value) {
            addCriterion("dormitory.waterId like", value, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdNotLike(String value) {
            addCriterion("dormitory.waterId not like", value, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdIn(List<String> values) {
            addCriterion("dormitory.waterId in", values, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdNotIn(List<String> values) {
            addCriterion("dormitory.waterId not in", values, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdBetween(String value1, String value2) {
            addCriterion("dormitory.waterId between", value1, value2, "waterId");
            return (Criteria) this;
        }

        public Criteria andWaterIdNotBetween(String value1, String value2) {
            addCriterion("dormitory.waterId not between", value1, value2, "waterId");
            return (Criteria) this;
        }

        public Criteria andPowerIdIsNull() {
            addCriterion("dormitory.powerId is null");
            return (Criteria) this;
        }

        public Criteria andPowerIdIsNotNull() {
            addCriterion("dormitory.powerId is not null");
            return (Criteria) this;
        }

        public Criteria andPowerIdEqualTo(String value) {
            addCriterion("dormitory.powerId =", value, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdNotEqualTo(String value) {
            addCriterion("dormitory.powerId <>", value, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdGreaterThan(String value) {
            addCriterion("dormitory.powerId >", value, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdGreaterThanOrEqualTo(String value) {
            addCriterion("dormitory.powerId >=", value, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdLessThan(String value) {
            addCriterion("dormitory.powerId <", value, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdLessThanOrEqualTo(String value) {
            addCriterion("dormitory.powerId <=", value, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdLike(String value) {
            addCriterion("dormitory.powerId like", value, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdNotLike(String value) {
            addCriterion("dormitory.powerId not like", value, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdIn(List<String> values) {
            addCriterion("dormitory.powerId in", values, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdNotIn(List<String> values) {
            addCriterion("dormitory.powerId not in", values, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdBetween(String value1, String value2) {
            addCriterion("dormitory.powerId between", value1, value2, "powerId");
            return (Criteria) this;
        }

        public Criteria andPowerIdNotBetween(String value1, String value2) {
            addCriterion("dormitory.powerId not between", value1, value2, "powerId");
            return (Criteria) this;
        }

        public Criteria andBasePowerIsNull() {
            addCriterion("dormitory.basePower is null");
            return (Criteria) this;
        }

        public Criteria andBasePowerIsNotNull() {
            addCriterion("dormitory.basePower is not null");
            return (Criteria) this;
        }

        public Criteria andBasePowerEqualTo(Short value) {
            addCriterion("dormitory.basePower =", value, "basePower");
            return (Criteria) this;
        }

        public Criteria andBasePowerNotEqualTo(Short value) {
            addCriterion("dormitory.basePower <>", value, "basePower");
            return (Criteria) this;
        }

        public Criteria andBasePowerGreaterThan(Short value) {
            addCriterion("dormitory.basePower >", value, "basePower");
            return (Criteria) this;
        }

        public Criteria andBasePowerGreaterThanOrEqualTo(Short value) {
            addCriterion("dormitory.basePower >=", value, "basePower");
            return (Criteria) this;
        }

        public Criteria andBasePowerLessThan(Short value) {
            addCriterion("dormitory.basePower <", value, "basePower");
            return (Criteria) this;
        }

        public Criteria andBasePowerLessThanOrEqualTo(Short value) {
            addCriterion("dormitory.basePower <=", value, "basePower");
            return (Criteria) this;
        }

        public Criteria andBasePowerIn(List<Short> values) {
            addCriterion("dormitory.basePower in", values, "basePower");
            return (Criteria) this;
        }

        public Criteria andBasePowerNotIn(List<Short> values) {
            addCriterion("dormitory.basePower not in", values, "basePower");
            return (Criteria) this;
        }

        public Criteria andBasePowerBetween(Short value1, Short value2) {
            addCriterion("dormitory.basePower between", value1, value2, "basePower");
            return (Criteria) this;
        }

        public Criteria andBasePowerNotBetween(Short value1, Short value2) {
            addCriterion("dormitory.basePower not between", value1, value2, "basePower");
            return (Criteria) this;
        }

        public Criteria andLivePowerIsNull() {
            addCriterion("dormitory.livePower is null");
            return (Criteria) this;
        }

        public Criteria andLivePowerIsNotNull() {
            addCriterion("dormitory.livePower is not null");
            return (Criteria) this;
        }

        public Criteria andLivePowerEqualTo(Short value) {
            addCriterion("dormitory.livePower =", value, "livePower");
            return (Criteria) this;
        }

        public Criteria andLivePowerNotEqualTo(Short value) {
            addCriterion("dormitory.livePower <>", value, "livePower");
            return (Criteria) this;
        }

        public Criteria andLivePowerGreaterThan(Short value) {
            addCriterion("dormitory.livePower >", value, "livePower");
            return (Criteria) this;
        }

        public Criteria andLivePowerGreaterThanOrEqualTo(Short value) {
            addCriterion("dormitory.livePower >=", value, "livePower");
            return (Criteria) this;
        }

        public Criteria andLivePowerLessThan(Short value) {
            addCriterion("dormitory.livePower <", value, "livePower");
            return (Criteria) this;
        }

        public Criteria andLivePowerLessThanOrEqualTo(Short value) {
            addCriterion("dormitory.livePower <=", value, "livePower");
            return (Criteria) this;
        }

        public Criteria andLivePowerIn(List<Short> values) {
            addCriterion("dormitory.livePower in", values, "livePower");
            return (Criteria) this;
        }

        public Criteria andLivePowerNotIn(List<Short> values) {
            addCriterion("dormitory.livePower not in", values, "livePower");
            return (Criteria) this;
        }

        public Criteria andLivePowerBetween(Short value1, Short value2) {
            addCriterion("dormitory.livePower between", value1, value2, "livePower");
            return (Criteria) this;
        }

        public Criteria andLivePowerNotBetween(Short value1, Short value2) {
            addCriterion("dormitory.livePower not between", value1, value2, "livePower");
            return (Criteria) this;
        }

        public Criteria andLiveWaterIsNull() {
            addCriterion("dormitory.liveWater is null");
            return (Criteria) this;
        }

        public Criteria andLiveWaterIsNotNull() {
            addCriterion("dormitory.liveWater is not null");
            return (Criteria) this;
        }

        public Criteria andLiveWaterEqualTo(Short value) {
            addCriterion("dormitory.liveWater =", value, "liveWater");
            return (Criteria) this;
        }

        public Criteria andLiveWaterNotEqualTo(Short value) {
            addCriterion("dormitory.liveWater <>", value, "liveWater");
            return (Criteria) this;
        }

        public Criteria andLiveWaterGreaterThan(Short value) {
            addCriterion("dormitory.liveWater >", value, "liveWater");
            return (Criteria) this;
        }

        public Criteria andLiveWaterGreaterThanOrEqualTo(Short value) {
            addCriterion("dormitory.liveWater >=", value, "liveWater");
            return (Criteria) this;
        }

        public Criteria andLiveWaterLessThan(Short value) {
            addCriterion("dormitory.liveWater <", value, "liveWater");
            return (Criteria) this;
        }

        public Criteria andLiveWaterLessThanOrEqualTo(Short value) {
            addCriterion("dormitory.liveWater <=", value, "liveWater");
            return (Criteria) this;
        }

        public Criteria andLiveWaterIn(List<Short> values) {
            addCriterion("dormitory.liveWater in", values, "liveWater");
            return (Criteria) this;
        }

        public Criteria andLiveWaterNotIn(List<Short> values) {
            addCriterion("dormitory.liveWater not in", values, "liveWater");
            return (Criteria) this;
        }

        public Criteria andLiveWaterBetween(Short value1, Short value2) {
            addCriterion("dormitory.liveWater between", value1, value2, "liveWater");
            return (Criteria) this;
        }

        public Criteria andLiveWaterNotBetween(Short value1, Short value2) {
            addCriterion("dormitory.liveWater not between", value1, value2, "liveWater");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}