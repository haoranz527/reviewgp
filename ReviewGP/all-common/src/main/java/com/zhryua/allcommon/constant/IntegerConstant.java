package com.zhryua.allcommon.constant;

public class IntegerConstant {

    public static final Integer ZERO = 0;

    public static final Integer ONE = 1;

    public static final Integer TWO = 2;

}
