package com.zhryua.allcommon.dto.serviceverificationcode.response;

import lombok.Data;

@Data
public class VerifyCodeResponse {

    private String code;

}
