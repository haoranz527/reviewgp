package com.zhryua.allcommon.api.serviceuser;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.SelectUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.UpdateUserRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SuppressWarnings("all")
public interface IUserService {

//    @PostMapping("/user/serviceuser/login")
//    public ResponseResult login(@RequestBody String userid
//            , @RequestBody String password
//            , @RequestBody long time);

    @PostMapping("/user/serviceuser/login")
    public ResponseResult login(@RequestBody String userid
            , @RequestParam(value = "password") String password
            , @RequestParam(value = "time") long time);

    @PostMapping("/user/serviceuser/insert")
    public ResponseResult insertUser(@RequestBody InsertUserRequest request);

    @PostMapping("/user/serviceuser/logout")
    public ResponseResult logOut(@RequestBody String token);

    @PostMapping("/user/serviceuser/update")
    public ResponseResult updateUser(@RequestBody UpdateUserRequest request);

    /**
     * 查询用户信息
     * 1、可查询字段：userid,班级，院系，姓名
     * ,邮件，手机号，身份证号，账号状态，宿舍号
     * ，创建时间范围，修改时间范围
     * 2、结果根据某字段排序：sortField
     * @param request
     * @return
     */
    @PostMapping("/user/serviceuser/select")
    public ResponseResult selectUser(SelectUserRequest request);

}
