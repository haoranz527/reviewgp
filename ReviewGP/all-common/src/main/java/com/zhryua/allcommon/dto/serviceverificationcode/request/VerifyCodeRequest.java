package com.zhryua.allcommon.dto.serviceverificationcode.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class VerifyCodeRequest {

    private String code;

    private int identity;

    private String phone;

}
