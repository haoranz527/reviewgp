package com.zhryua.allcommon.pojo.database;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 
 * 
 */
@Table(name="person")
@Data
public class Person implements Serializable {
    @Id
    @GeneratedValue
    private String bizid;

    private String name;

    private Integer age;

    private String phone;

    private static final long serialVersionUID = 1L;
}