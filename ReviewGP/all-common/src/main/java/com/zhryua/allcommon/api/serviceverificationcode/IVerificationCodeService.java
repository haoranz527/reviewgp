package com.zhryua.allcommon.api.serviceverificationcode;

import org.springframework.web.bind.annotation.*;

@RestController
public interface IVerificationCodeService {
//
//    @PostMapping("/user/serviceCode/generatorCode")
//    public ResponseResult generatorCode(@RequestBody int identity, @RequestParam(value = "phone") String phone);
//
//    @PostMapping("/user/serviceCode/verifyCode")
//    public ResponseResult verifyCode(@RequestBody int identity, @RequestParam(value = "phone") String phone, @RequestParam(value = "code") String code);

    @PostMapping("/getCodeByPhone")
    public String getCodeByPhone(@RequestParam(value = "phone") String phone);

    @PostMapping("/getCodeByEmail")
    public String getCodeByEmail(@RequestParam(value = "email") String email);

    // 验证验证码是否正确
    @PostMapping("/verCode")
    public boolean verCode(@RequestParam(value = "path") String path, @RequestParam(value = "code") String code);
}
