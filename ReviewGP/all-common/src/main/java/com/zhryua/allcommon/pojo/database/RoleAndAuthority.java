package com.zhryua.allcommon.pojo.database;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 
 * 
 */
@Table(name="role_and_authority")
@Data
public class RoleAndAuthority implements Serializable {
    @Id
    @GeneratedValue
    private String roleId;

    private String authorityId;

    private static final long serialVersionUID = 1L;
}