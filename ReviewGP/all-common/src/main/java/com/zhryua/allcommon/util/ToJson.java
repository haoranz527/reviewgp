package com.zhryua.allcommon.util;

import com.alibaba.fastjson.JSON;
import com.zhryua.allcommon.pojo.database.User;

public class ToJson {

    public static String toJson(Object o) {
        String s = JSON.toJSONString(o);
        return s;
    }

    public static void main(String[] args) {
        User user = new User();
        user.setPassword("1234").setWechat("sdsas").setQq("1243");
        String s = toJson(user);
        System.out.println(s);
    }

}
