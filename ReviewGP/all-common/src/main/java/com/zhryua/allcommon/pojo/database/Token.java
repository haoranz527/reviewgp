package com.zhryua.allcommon.pojo.database;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 
 * 
 */
@Table(name="token")
@Data
public class Token implements Serializable {
    @Id
    @GeneratedValue
    private String bizId;

    private String token;

    @NotEmpty
    private String userId;

    private Date utm;

    private Date ctm;

    private static final long serialVersionUID = 1L;
}