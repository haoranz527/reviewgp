package com.zhryua.allcommon.dto.serviceuser.request;

import com.zhryua.allcommon.dto.basepage.BasePageRequest;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class SelectUserRequest extends BasePageRequest {
    private String userId;

    private String name;

    private String email;

    private String phone;

    // 身份证号
    private String identity;

    /**
     * 0-待审核1-账号正常使用2-账号注销3-待审核
     */
    private Integer status;

    // 宿舍号
    private String dormitoryId;

    private List<String> createTime;

    private List<String> updateTime;

    // 班级
    private Integer classNumber;

    // 院系
    private Integer department;

    // 排序字段
    private String orderBy;

}
