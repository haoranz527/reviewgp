package com.zhryua.allcommon.constant;

import lombok.Data;

@Data
public class IdentityConstant {

    public static final int TEACHER = 1;

    public static final int STUDENT = 2;

}
