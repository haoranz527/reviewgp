package com.zhryua.allcommon.pojo.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

public class TradeHistoryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public TradeHistoryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBizIdIsNull() {
            addCriterion("trade_history.bizId is null");
            return (Criteria) this;
        }

        public Criteria andBizIdIsNotNull() {
            addCriterion("trade_history.bizId is not null");
            return (Criteria) this;
        }

        public Criteria andBizIdEqualTo(String value) {
            addCriterion("trade_history.bizId =", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotEqualTo(String value) {
            addCriterion("trade_history.bizId <>", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdGreaterThan(String value) {
            addCriterion("trade_history.bizId >", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdGreaterThanOrEqualTo(String value) {
            addCriterion("trade_history.bizId >=", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdLessThan(String value) {
            addCriterion("trade_history.bizId <", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdLessThanOrEqualTo(String value) {
            addCriterion("trade_history.bizId <=", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdLike(String value) {
            addCriterion("trade_history.bizId like", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotLike(String value) {
            addCriterion("trade_history.bizId not like", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdIn(List<String> values) {
            addCriterion("trade_history.bizId in", values, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotIn(List<String> values) {
            addCriterion("trade_history.bizId not in", values, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdBetween(String value1, String value2) {
            addCriterion("trade_history.bizId between", value1, value2, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotBetween(String value1, String value2) {
            addCriterion("trade_history.bizId not between", value1, value2, "bizId");
            return (Criteria) this;
        }

        public Criteria andEtmIsNull() {
            addCriterion("trade_history.etm is null");
            return (Criteria) this;
        }

        public Criteria andEtmIsNotNull() {
            addCriterion("trade_history.etm is not null");
            return (Criteria) this;
        }

        public Criteria andEtmEqualTo(Date value) {
            addCriterion("trade_history.etm =", value, "etm");
            return (Criteria) this;
        }

        public Criteria andEtmNotEqualTo(Date value) {
            addCriterion("trade_history.etm <>", value, "etm");
            return (Criteria) this;
        }

        public Criteria andEtmGreaterThan(Date value) {
            addCriterion("trade_history.etm >", value, "etm");
            return (Criteria) this;
        }

        public Criteria andEtmGreaterThanOrEqualTo(Date value) {
            addCriterion("trade_history.etm >=", value, "etm");
            return (Criteria) this;
        }

        public Criteria andEtmLessThan(Date value) {
            addCriterion("trade_history.etm <", value, "etm");
            return (Criteria) this;
        }

        public Criteria andEtmLessThanOrEqualTo(Date value) {
            addCriterion("trade_history.etm <=", value, "etm");
            return (Criteria) this;
        }

        public Criteria andEtmIn(List<Date> values) {
            addCriterion("trade_history.etm in", values, "etm");
            return (Criteria) this;
        }

        public Criteria andEtmNotIn(List<Date> values) {
            addCriterion("trade_history.etm not in", values, "etm");
            return (Criteria) this;
        }

        public Criteria andEtmBetween(Date value1, Date value2) {
            addCriterion("trade_history.etm between", value1, value2, "etm");
            return (Criteria) this;
        }

        public Criteria andEtmNotBetween(Date value1, Date value2) {
            addCriterion("trade_history.etm not between", value1, value2, "etm");
            return (Criteria) this;
        }

        public Criteria andAmountIsNull() {
            addCriterion("trade_history.amount is null");
            return (Criteria) this;
        }

        public Criteria andAmountIsNotNull() {
            addCriterion("trade_history.amount is not null");
            return (Criteria) this;
        }

        public Criteria andAmountEqualTo(Long value) {
            addCriterion("trade_history.amount =", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotEqualTo(Long value) {
            addCriterion("trade_history.amount <>", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThan(Long value) {
            addCriterion("trade_history.amount >", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThanOrEqualTo(Long value) {
            addCriterion("trade_history.amount >=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThan(Long value) {
            addCriterion("trade_history.amount <", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThanOrEqualTo(Long value) {
            addCriterion("trade_history.amount <=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountIn(List<Long> values) {
            addCriterion("trade_history.amount in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotIn(List<Long> values) {
            addCriterion("trade_history.amount not in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountBetween(Long value1, Long value2) {
            addCriterion("trade_history.amount between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotBetween(Long value1, Long value2) {
            addCriterion("trade_history.amount not between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andItemIsNull() {
            addCriterion("trade_history.item is null");
            return (Criteria) this;
        }

        public Criteria andItemIsNotNull() {
            addCriterion("trade_history.item is not null");
            return (Criteria) this;
        }

        public Criteria andItemEqualTo(String value) {
            addCriterion("trade_history.item =", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotEqualTo(String value) {
            addCriterion("trade_history.item <>", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemGreaterThan(String value) {
            addCriterion("trade_history.item >", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemGreaterThanOrEqualTo(String value) {
            addCriterion("trade_history.item >=", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemLessThan(String value) {
            addCriterion("trade_history.item <", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemLessThanOrEqualTo(String value) {
            addCriterion("trade_history.item <=", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemLike(String value) {
            addCriterion("trade_history.item like", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotLike(String value) {
            addCriterion("trade_history.item not like", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemIn(List<String> values) {
            addCriterion("trade_history.item in", values, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotIn(List<String> values) {
            addCriterion("trade_history.item not in", values, "item");
            return (Criteria) this;
        }

        public Criteria andItemBetween(String value1, String value2) {
            addCriterion("trade_history.item between", value1, value2, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotBetween(String value1, String value2) {
            addCriterion("trade_history.item not between", value1, value2, "item");
            return (Criteria) this;
        }

        public Criteria andConsumptionIsNull() {
            addCriterion("trade_history.consumption is null");
            return (Criteria) this;
        }

        public Criteria andConsumptionIsNotNull() {
            addCriterion("trade_history.consumption is not null");
            return (Criteria) this;
        }

        public Criteria andConsumptionEqualTo(Integer value) {
            addCriterion("trade_history.consumption =", value, "consumption");
            return (Criteria) this;
        }

        public Criteria andConsumptionNotEqualTo(Integer value) {
            addCriterion("trade_history.consumption <>", value, "consumption");
            return (Criteria) this;
        }

        public Criteria andConsumptionGreaterThan(Integer value) {
            addCriterion("trade_history.consumption >", value, "consumption");
            return (Criteria) this;
        }

        public Criteria andConsumptionGreaterThanOrEqualTo(Integer value) {
            addCriterion("trade_history.consumption >=", value, "consumption");
            return (Criteria) this;
        }

        public Criteria andConsumptionLessThan(Integer value) {
            addCriterion("trade_history.consumption <", value, "consumption");
            return (Criteria) this;
        }

        public Criteria andConsumptionLessThanOrEqualTo(Integer value) {
            addCriterion("trade_history.consumption <=", value, "consumption");
            return (Criteria) this;
        }

        public Criteria andConsumptionIn(List<Integer> values) {
            addCriterion("trade_history.consumption in", values, "consumption");
            return (Criteria) this;
        }

        public Criteria andConsumptionNotIn(List<Integer> values) {
            addCriterion("trade_history.consumption not in", values, "consumption");
            return (Criteria) this;
        }

        public Criteria andConsumptionBetween(Integer value1, Integer value2) {
            addCriterion("trade_history.consumption between", value1, value2, "consumption");
            return (Criteria) this;
        }

        public Criteria andConsumptionNotBetween(Integer value1, Integer value2) {
            addCriterion("trade_history.consumption not between", value1, value2, "consumption");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("trade_history.`status` is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("trade_history.`status` is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("trade_history.`status` =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("trade_history.`status` <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("trade_history.`status` >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("trade_history.`status` >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("trade_history.`status` <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("trade_history.`status` <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("trade_history.`status` in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("trade_history.`status` not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("trade_history.`status` between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("trade_history.`status` not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("trade_history.address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("trade_history.address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("trade_history.address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("trade_history.address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("trade_history.address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("trade_history.address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("trade_history.address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("trade_history.address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("trade_history.address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("trade_history.address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("trade_history.address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("trade_history.address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("trade_history.address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("trade_history.address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andProcessorIdIsNull() {
            addCriterion("trade_history.processorId is null");
            return (Criteria) this;
        }

        public Criteria andProcessorIdIsNotNull() {
            addCriterion("trade_history.processorId is not null");
            return (Criteria) this;
        }

        public Criteria andProcessorIdEqualTo(String value) {
            addCriterion("trade_history.processorId =", value, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdNotEqualTo(String value) {
            addCriterion("trade_history.processorId <>", value, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdGreaterThan(String value) {
            addCriterion("trade_history.processorId >", value, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdGreaterThanOrEqualTo(String value) {
            addCriterion("trade_history.processorId >=", value, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdLessThan(String value) {
            addCriterion("trade_history.processorId <", value, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdLessThanOrEqualTo(String value) {
            addCriterion("trade_history.processorId <=", value, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdLike(String value) {
            addCriterion("trade_history.processorId like", value, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdNotLike(String value) {
            addCriterion("trade_history.processorId not like", value, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdIn(List<String> values) {
            addCriterion("trade_history.processorId in", values, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdNotIn(List<String> values) {
            addCriterion("trade_history.processorId not in", values, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdBetween(String value1, String value2) {
            addCriterion("trade_history.processorId between", value1, value2, "processorId");
            return (Criteria) this;
        }

        public Criteria andProcessorIdNotBetween(String value1, String value2) {
            addCriterion("trade_history.processorId not between", value1, value2, "processorId");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}