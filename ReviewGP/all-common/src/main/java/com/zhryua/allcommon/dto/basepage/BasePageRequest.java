package com.zhryua.allcommon.dto.basepage;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class BasePageRequest {

    private Integer pageSize;

    private Integer pageNumber;

}
