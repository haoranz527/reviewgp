package com.zhryua.allcommon.pojo.database;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 
 * 
 */
@Table(name="card")
@Data
@Accessors(chain = true)
public class Card implements Serializable {
    @Id
    @GeneratedValue
    private String bizId;

    private Integer status;

    private String userId;

    private Date mtm;

    private Date atm;

    private Long limit;

    private Long balance;

    private static final long serialVersionUID = 1L;
}