package com.zhryua.allcommon.api.serviceconsume;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceconsume.request.ConsumRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface IConsumeService {

    /**
     * 消费扣款
     * @param request
     * @return
     */
    @PostMapping("/consume/debit")
    public ResponseResult debit(@RequestBody ConsumRequest request);

}
