package com.zhryua.allcommon.api.servicepayment;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicepayment.request.PayRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * service-payment服务仅是对card进行充值时服务
 */
public interface IPaymentApi {
    @PostMapping("/pay/alipay")
    public ResponseResult AliPay(@RequestBody PayRequest request);

    @PostMapping("/pay/wechat")
    public ResponseResult WechatPay(@RequestBody PayRequest request);

    @PostMapping("/pay/money")
    public ResponseResult MoneyPay(@RequestBody PayRequest request);

}





