package com.zhryua.allcommon.api.servicesms;

import com.zhryua.allcommon.dto.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface IServiceSms {

    @PostMapping("/user/sendSms")
    public ResponseResult sendSms(@RequestBody String phone,@RequestParam(value = "code") String code);

}
