package com.zhryua.allcommon.dto.servicecard.request;

import com.zhryua.allcommon.dto.basepage.BasePageRequest;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class SelectCardRequest extends BasePageRequest {

    private String userId;

    private List<Long> balances;

    private List<String> creatTime;

    private List<String> updateTime;

    private List<Long> limits;

}
