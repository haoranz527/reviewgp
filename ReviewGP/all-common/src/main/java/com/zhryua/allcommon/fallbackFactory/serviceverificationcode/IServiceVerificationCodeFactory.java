package com.zhryua.allcommon.fallbackFactory.serviceverificationcode;

import com.zhryua.allcommon.api.serviceverificationcode.IVerificationCodeService;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.fallbackFactory.servicecard.IServiceCardFallBackFactory;
import feign.hystrix.FallbackFactory;

public class IServiceVerificationCodeFactory implements FallbackFactory<IVerificationCodeService> {
    @Override
    public IVerificationCodeService create(Throwable throwable) {
        return new IVerificationCodeService() {
//            @Override
//            public ResponseResult generatorCode(int identity, String phone) {
//                return ResponseResult.fail("进入熔断！");
//            }
//
//            @Override
//            public ResponseResult verifyCode(int identity, String phone, String code) {
//                return ResponseResult.fail("进入熔断！");
//            }

            @Override
            public String getCodeByPhone(String phone) {
                return "NULL!";
            }

            @Override
            public String getCodeByEmail(String email) {
                return "NULL!";
            }

            @Override
            public boolean verCode(String path, String code) {
                return false;
            }
        };
    }
}
