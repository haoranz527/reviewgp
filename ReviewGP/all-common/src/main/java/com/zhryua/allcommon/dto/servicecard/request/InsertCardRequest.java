package com.zhryua.allcommon.dto.servicecard.request;

import com.zhryua.allcommon.pojo.database.Card;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class InsertCardRequest extends Card {
}
