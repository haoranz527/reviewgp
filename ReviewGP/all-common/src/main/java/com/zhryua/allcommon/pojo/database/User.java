package com.zhryua.allcommon.pojo.database;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 
 * 
 */
@Table(name="user")
@Data
@Accessors(chain = true)
public class User implements Serializable {
    @Id
    @GeneratedValue
    private String bizId;

    private String name;

    private String phone;

    private String password;

    private String email;

    private String qq;

    private String wechat;

    private Integer department;

    private Integer classNumber;

    private String identity;

    private Integer is_authentication;

    private Date ctm;

    private Date utm;

    /**
     * 0-待审核1-账号正常使用2-账号注销3-待审核
     */
    private Integer status;

    private String cardId;

    private String auditorId;

    private Integer subApp;


    private String dormitoryId;

    private Short bed;

    private String userId;

    private String image;

    private static final long serialVersionUID = 1L;
}