package com.zhryua.allcommon.constant;

public class RedisKeyPrefixConstant {

    /**
     * 学生登录redis前缀
     */
    public static final String STUDENT_LOGIN_CODE_KEY_PRE = "student_login_code_key_pre_";

    /**
     * 教师登录redis前缀
     */
    public static final String TEACHER_LOGIN_CODE_KEY_PRE = "teacher_login_code_key_pre_";

    /**
     * 学生未经过验证
     */
    public static final String STUDENT_NOT_CERTIFIED = "student_not_certified_";

}
