package com.zhryua.allcommon.fallbackFactory.serviceuser;

import com.zhryua.allcommon.api.serviceuser.IUserService;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.serviceuser.request.InsertUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.SelectUserRequest;
import com.zhryua.allcommon.dto.serviceuser.request.UpdateUserRequest;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class IServiceUserFallBackFactory implements FallbackFactory<IUserService> {
    @Override
    public IUserService create(Throwable throwable) {
        log.info("进入{}, 开始熔断", this.getClass().getName());

        return new IUserService() {
//            @Override
//            public ResponseResult login(String userid, String password, long time) {
//                return ResponseResult.fail("进入熔断！");
//            }

            @Override
            public ResponseResult login(String userid, String password, long time) {
                return ResponseResult.fail("进入熔断！");
            }
            @Override
            public ResponseResult insertUser(InsertUserRequest request) {
                return ResponseResult.fail("进入熔断！");

            }

            @Override
            public ResponseResult logOut(String token) {
                return ResponseResult.fail("进入熔断！");

            }

            @Override
            public ResponseResult updateUser(UpdateUserRequest request) {
                return ResponseResult.fail("进入熔断！");

            }

            @Override
            public ResponseResult selectUser(SelectUserRequest request) {
                return ResponseResult.fail("进入熔断！");

            }
        };
    }
}
