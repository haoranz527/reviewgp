package com.zhryua.allcommon.dto.servicepayment.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@SuppressWarnings("all")
public class PayRequest {

    private int type;


    private String account;


    private Long amount;

}
