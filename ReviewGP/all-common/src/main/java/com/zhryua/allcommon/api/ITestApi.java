package com.zhryua.allcommon.api;

import org.springframework.web.bind.annotation.GetMapping;

public interface ITestApi {

    @GetMapping("/getHello")
    public String getHello() ;

}
