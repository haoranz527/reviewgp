package com.zhryua.allcommon.constant;

import lombok.Getter;

public enum ResponseConstantEnum {

    FAIL(-1, "失败"),

    INTERNALERROR(-2, "internal error"),

    SUCCESS(1, "成功");

    @Getter
    private final int code;

    @Getter
    private final String msg;

    ResponseConstantEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
