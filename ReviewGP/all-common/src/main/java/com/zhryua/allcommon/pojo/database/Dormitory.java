package com.zhryua.allcommon.pojo.database;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 
 * 
 */
@Table(name="dormitory")
@Data
public class Dormitory implements Serializable {
    @Id
    @GeneratedValue
    private String bizId;

    private Short status;

    private Short people;

    private Short repairs;

    private String item;

    private Date ctm;

    private Integer useTime;

    private Long power;

    private Long water;

    private String principalId;

    private String airConditionId;

    private String waterId;

    private String powerId;

    private Short basePower;

    private Short livePower;

    private Short liveWater;

    private static final long serialVersionUID = 1L;
}