package com.zhryua.allcommon.constant;

// 交易形式
public class PaymentTypeConstant {
    // 1支付宝 2微信 3现金
    public static final Integer ALIPAY = 1;

    public static final Integer WECHAT = 2;

    public static final Integer MONEY = 3;
}
