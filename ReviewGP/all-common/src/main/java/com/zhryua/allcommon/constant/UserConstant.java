package com.zhryua.allcommon.constant;

public class UserConstant {

    /**
     * 未认证
     */
    public static final int NOT_CERTIFIED = 0;

    /**
     * 已认证
     */
    public static final int VERIFIED = 1;


}
