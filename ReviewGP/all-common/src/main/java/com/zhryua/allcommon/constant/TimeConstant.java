package com.zhryua.allcommon.constant;

public class TimeConstant {

    // 一个月
    public static final Long ONE_MONTH = 1000 * 60 * 60 * 24 * 30L;
    // 一天
    public static final Long ONE_DAY = 1000 * 60 * 24L;
    // 2分钟
    public static final Long TWO_MINUTES = 1000 * 2L;
}
