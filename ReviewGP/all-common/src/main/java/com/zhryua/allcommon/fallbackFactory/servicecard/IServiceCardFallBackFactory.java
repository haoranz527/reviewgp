package com.zhryua.allcommon.fallbackFactory.servicecard;

import com.zhryua.allcommon.api.servicecard.ICardService;
import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicecard.request.InsertCardRequest;
import com.zhryua.allcommon.dto.servicecard.request.SelectCardRequest;
import com.zhryua.allcommon.dto.servicecard.request.UpdateCardRequest;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class IServiceCardFallBackFactory implements FallbackFactory<ICardService> {
    @Override
    public ICardService create(Throwable throwable) {
        log.info("进入{},开始熔断", this.getClass().getName());
        return new ICardService() {
            @Override
            public ResponseResult insertCard(InsertCardRequest request) {
                return ResponseResult.fail("进入熔断！");
            }

            @Override
            public ResponseResult UpdateCard(UpdateCardRequest request) {
                return ResponseResult.fail("进入熔断！");
            }

            @Override
            public ResponseResult selectCard(SelectCardRequest request) {
                return ResponseResult.fail("进入熔断！");
            }
        };
    }
}
