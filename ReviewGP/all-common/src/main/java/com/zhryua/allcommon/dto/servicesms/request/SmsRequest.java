package com.zhryua.allcommon.dto.servicesms.request;

import com.zhryua.allcommon.dto.servicesms.SmsTemplateDto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class SmsRequest {

    // 目标设备号，可默认为手机，后期可用邮箱
    private String[] receivers;

    // 要发送的信息模板
    private List<SmsTemplateDto> data;

}
