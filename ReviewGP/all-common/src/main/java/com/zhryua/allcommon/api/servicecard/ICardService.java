package com.zhryua.allcommon.api.servicecard;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicecard.request.InsertCardRequest;
import com.zhryua.allcommon.dto.servicecard.request.SelectCardRequest;
import com.zhryua.allcommon.dto.servicecard.request.UpdateCardRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


public interface ICardService{

    @PostMapping("/card/insertCard")
    public ResponseResult insertCard(@RequestBody InsertCardRequest request);

    /**
     * 支持更新status，limit，balance
     * @param request
     * @return
     */
    @PostMapping("/card/updateCard")
    public ResponseResult UpdateCard(@RequestBody UpdateCardRequest request);


    /**
     * 支持按照余额范围查找
     * userId查找
     * 创建日期范围查找
     * 修改日期范围查找
     * 限制范围查找
     * @return
     */
    @PostMapping("/card/selectCard")
    public ResponseResult selectCard(@RequestBody SelectCardRequest request);

}
