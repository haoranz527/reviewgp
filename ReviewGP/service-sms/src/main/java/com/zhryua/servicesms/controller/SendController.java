package com.zhryua.servicesms.controller;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicesms.request.SmsRequest;
import com.zhryua.servicesms.service.ISmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/send")
public class SendController {

    @Autowired
    private ISmsService smsService;

    @PostMapping("/sms-template")
    public ResponseResult send(@RequestBody SmsRequest request) {
        return smsService.sendSms(request);
    }

}
