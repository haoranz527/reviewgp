package com.zhryua.servicesms.service.impl;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicesms.request.SmsRequest;
import com.zhryua.servicesms.service.ISmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SmsServiceImpl implements ISmsService {
    @Override
    public ResponseResult sendSms(SmsRequest request) {
        Object code = request.getData().get(0).getTemplateMap().get("code");
        log.info("验证码{code}已发送", code);
        return ResponseResult.success("发送成功");
    }

}
