package com.zhryua.servicesms.service;

import com.zhryua.allcommon.dto.ResponseResult;

public interface IEmailService {

    public ResponseResult sendMail(String email);

}
