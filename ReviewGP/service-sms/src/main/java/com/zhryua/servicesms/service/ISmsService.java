package com.zhryua.servicesms.service;

import com.zhryua.allcommon.dto.ResponseResult;
import com.zhryua.allcommon.dto.servicesms.request.SmsRequest;

public interface ISmsService {

    public ResponseResult sendSms(SmsRequest request);

}
